import 'package:chokutsuflutter/blocks/cptdb_tile.dart';
import 'package:chokutsuflutter/blocks/icon_text.dart';
import 'package:chokutsuflutter/blocks/select_route.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class APage extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;

  const APage(this.client, this.agency, {Key? key}) : super(key: key);

  @override
  _APageState createState() => _APageState(this.client, this.agency);
}

class _APageState extends State<APage> {
  final grpc.DataClient client;
  final grpc.Agency agency;

  _APageState(this.client, this.agency);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Thing(Icons.business, "a${agency.tag}", agency.title),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => setState(() {}),
        tooltip: S.of(context).refresh,
        child: Icon(Icons.refresh),
      ), // This trailing comma makes auto-formatting nicer for build methods.
      body: Column(
        children: [
          CPTDBTile(agency.title),
          Expanded(
            child: SelectRoute(client, agency),
          ),
        ],
      ),
    );
  }
}
