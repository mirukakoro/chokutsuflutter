import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/blocks/icon_text.dart';
import 'package:chokutsuflutter/blocks/map_view.dart';
import 'package:chokutsuflutter/blocks/s_preds.dart';
import 'package:chokutsuflutter/blocks/s_view.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class SPage extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.Stop sInit;
  final grpc.Vehicle? vOnly;
  final int initialIndex;

  const SPage(this.client, this.agency, this.route, this.sInit,
      {this.vOnly, this.initialIndex = 0, Key? key})
      : super(key: key);

  @override
  _SPageState createState() =>
      _SPageState(this.client, this.agency, this.route, this.sInit,
          vOnly: this.vOnly,
          initialIndex: this.initialIndex);
}

class _SPageState extends State<SPage> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.Stop sInit;
  final grpc.Vehicle? vOnly;
  final int initialIndex;

  _SPageState(this.client, this.agency, this.route, this.sInit,
      {this.vOnly, this.initialIndex = 0});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: initialIndex,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Thing(Icons.trip_origin, "s${sInit.tag}", sInit.title),
          bottom: TabBar(tabs: [
            Tab(icon: Icon(Icons.info)),
            Tab(icon: Icon(Icons.map)),
            Tab(icon: Icon(Icons.timeline)),
          ]),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => setState(() {}),
          tooltip: S
              .of(context)
              .refresh,
          child: Icon(Icons.refresh),
        ),
        body: TabBarView(children: [
          SView(client, agency, route, sInit),
          FutureBuilder(
            future: client.route(
                grpc.RouteReq(agencyTag: agency.tag, routeTag: route.tag)),
            builder:
                (BuildContext context, AsyncSnapshot<grpc.RouteResp> snapshot) {
              if (snapshot.connectionState != ConnectionState.done) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (snapshot.hasError) {
                return ErrorView(snapshot.error.toString());
              }
              if (!snapshot.hasData) {
                return ErrorNodataView();
              }
              for (final stopCand in snapshot.data!.route.stops) {
                if (stopCand.tag == sInit.tag) {
                  return MapView(stopCand.pos.lat, stopCand.pos.lon);
                }
              }
              return ErrorNodataView();
            },
          ),
          SPreds(client, agency, route, sInit, vOnly: vOnly),
        ]),
      ),
    );
  }
}
