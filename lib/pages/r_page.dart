import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/blocks/icon_text.dart';
import 'package:chokutsuflutter/blocks/m_view.dart';
import 'package:chokutsuflutter/blocks/r_view.dart';
import 'package:chokutsuflutter/blocks/select_direction.dart';
import 'package:chokutsuflutter/blocks/select_stop.dart';
import 'package:chokutsuflutter/blocks/select_vehicle.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class RPage extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;

  const RPage(this.client, this.agency, this.route, {Key? key})
      : super(key: key);

  @override
  _RPageState createState() =>
      _RPageState(this.client, this.agency, this.route);
}

class _RPageState extends State<RPage> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;

  _RPageState(this.client, this.agency, this.route);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: AppBar(
          title: Thing(Icons.moving, "r${route.tag}", route.title),
          bottom: TabBar(tabs: [
            Tab(icon: Icon(Icons.info)),
            Tab(icon: Icon(Icons.chat)),
            Tab(icon: Icon(Icons.map)),
            Tab(icon: Icon(Icons.import_export)),
            Tab(icon: Icon(Icons.directions_transit)),
            Tab(icon: Icon(Icons.trip_origin)),
          ]),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => setState(() {}),
          tooltip: S.of(context).refresh,
          child: Icon(Icons.refresh),
        ),
        body: TabBarView(children: [
          RView(client, agency, route),
          MView(client, agency, route),
          Text("map"),
          SelectDirection(client, agency, route),
          FutureBuilder(
              future: client.route(grpc.RouteReq(
                agencyTag: agency.tag,
                routeTag: route.tag,
              )),
              builder: (BuildContext context,
                  AsyncSnapshot<grpc.RouteResp> snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (snapshot.hasError) {
                  return ErrorView(snapshot.error.toString());
                }
                if (!snapshot.hasData) {
                  return ErrorNodataView();
                }
                return SelectVehicle(client, agency, snapshot.data!.route);
              }),
          SelectStop(client, agency, route),
        ]),
      ),
    );
  }
}
