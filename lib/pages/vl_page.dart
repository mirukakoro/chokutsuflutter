import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/blocks/icon_text.dart';
import 'package:chokutsuflutter/blocks/map_view.dart';
import 'package:chokutsuflutter/blocks/v_preds.dart';
import 'package:chokutsuflutter/blocks/vl_view.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class VLPage extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.VehicleLocation vlInit;
  final String vehicleTag;

  const VLPage(
      this.client, this.agency, this.route, this.vlInit, this.vehicleTag,
      {Key? key})
      : super(key: key);

  @override
  _VLPageState createState() => _VLPageState(
      this.client, this.agency, this.route, this.vlInit, this.vehicleTag);
}

class _VLPageState extends State<VLPage> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.VehicleLocation vlInit;
  final String vehicleTag;

  _VLPageState(
      this.client, this.agency, this.route, this.vlInit, this.vehicleTag);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Thing(Icons.directions_transit, "v$vehicleTag", ""),
          bottom: TabBar(tabs: [
            Tab(icon: Icon(Icons.info)),
            Tab(icon: Icon(Icons.map)),
            Tab(icon: Icon(Icons.timeline)),
          ]),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => setState(() {}),
          tooltip: S.of(context).refresh,
          child: Icon(Icons.refresh),
        ),
        body: TabBarView(children: [
          VlView(client, agency, route, vlInit, vehicleTag),
          FutureBuilder(
            future: client.locations(grpc.LocationsReq(
                agencyTag: agency.tag, vehicleTags: [vehicleTag])),
            builder: (BuildContext context,
                AsyncSnapshot<grpc.LocationsResp> snapshot) {
              if (snapshot.connectionState != ConnectionState.done) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (snapshot.hasError) {
                return ErrorView(snapshot.error.toString());
              }
              if (!snapshot.hasData || snapshot.data!.locations.length == 0) {
                return ErrorNodataView();
              }

              grpc.VehicleLocation vl = snapshot.data!.locations[0];

              return MapView(vl.location.lat, vl.location.lon);
            },
          ),
          VPreds(client, agency, route, vlInit.target)
        ]),
      ),
    );
  }
}
