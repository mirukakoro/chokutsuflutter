import 'package:chokutsuflutter/blocks/select_agency.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:grpc/grpc.dart' as grpc;
import 'generated/l10n.dart';
import 'about.dart';

const channelHost = 'localhost';
const channelPort = 5000;

Future<void> main() async {
  final channel = grpc.ClientChannel(
    channelHost,
    port: channelPort,
    options: const grpc.ChannelOptions(
        credentials: grpc.ChannelCredentials.insecure()),
  );
  final client = DataClient(channel);

  runApp(App(client));
}

class App extends StatelessWidget {
  final DataClient client;

  App(this.client);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        AppLocalizationDelegate(),
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('ja'),
      ],
      title: 'Chokutsu',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      initialRoute: HomePage.id,
      routes: {
        HomePage.id: (context) => HomePage(client),
        AboutPage.id: (context) => AboutPage(client),
      },
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage(this.client) : super();

  static const id = '/';
  final DataClient client;

  @override
  _HomePageState createState() => _HomePageState(client);
}

class _HomePageState extends State<HomePage> {
  _HomePageState(this.client);

  final DataClient client;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appTitle),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => setState(() {}),
        tooltip: S.of(context).refresh,
        child: Icon(Icons.refresh),
      ), // This trailing comma makes auto-formatting nicer for build methods.
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, AboutPage.id);
              },
              icon: Icon(Icons.info),
            ),
            Expanded(
              child: SelectAgency(client),
            ),
          ],
        ),
      ),
    );
  }
}
