import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class MapView extends StatefulWidget {
  final double lat;
  final double lon;

  const MapView(this.lat, this.lon, {Key? key}) : super(key: key);

  @override
  _MapViewState createState() => _MapViewState(this.lat, this.lon);
}

class _MapViewState extends State<MapView> {
  final double lat;
  final double lon;

  _MapViewState(this.lat, this.lon);

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        center: LatLng(lat,lon),
        zoom: 15.0,
      ),
      layers: [
        TileLayerOptions(
          urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
          subdomains: ['a', 'b', 'c'],
          tileProvider: NonCachingNetworkTileProvider(),
        ),
        MarkerLayerOptions(markers: [
          Marker(
            width: 80.0,
            height: 80.0,
            point: LatLng(lat, lon),
            builder: (ctx) => Icon(Icons.place),
          ),
        ])
      ],
    );
  }
}
