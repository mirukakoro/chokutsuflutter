import 'package:chokutsuflutter/pages/r_page.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;
import 'package:flutter/material.dart';

import 'licensing_tile.dart';

class SelectRoute extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;

  const SelectRoute(this.client, this.agency, {Key? key}) : super(key: key);

  @override
  _SelectRouteState createState() =>
      _SelectRouteState(this.client, this.agency);
}

class _SelectRouteState extends State<SelectRoute> {
  final grpc.DataClient client;
  final grpc.Agency agency;

  _SelectRouteState(this.client, this.agency);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.routes(grpc.RoutesReq(agencyTag: agency.tag)),
      builder: (BuildContext context, AsyncSnapshot<grpc.RoutesResp> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return ListView(
          children: [
            for (grpc.Route route in snapshot.data!.routes)
              ListTile(
                title: Text(
                  route.title,
                ),
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            RPage(this.client, agency, route))),
              ),
            LicensingTile(snapshot.data!.licensing),
          ],
        );
      },
    );
  }
}
