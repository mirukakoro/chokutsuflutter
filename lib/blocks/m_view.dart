import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/blocks/licensing_tile.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class MView extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  const MView(this.client, this.agency, this.route,{Key? key}) : super(key: key);

  @override
  _MViewState createState() => _MViewState(this.client, this.agency, this.route);
}

class _MViewState extends State<MView> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;

  _MViewState(this.client, this.agency, this.route);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.messages(grpc.MessagesReq(
        agencyTag: agency.tag,
        routeTags: [route.tag],
      )),
      builder:
          (BuildContext context, AsyncSnapshot<grpc.MessagesResp> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return ErrorView(snapshot.error.toString());
        }
        if (!snapshot.hasData) {
          return ErrorNodataView();
        }

        return ListView(
          children: [
            for (grpc.Message msg in snapshot.data!.messages)
              Builder(builder: (BuildContext context) {
                return ListTile(
                  title: Text(msg.text),
                  subtitle: Text(msg.textAlt),
                  trailing: Text(
                    "m${msg.tag}",
                    style: TextStyle(fontFamily: "monospace"),
                  ),
                  // onTap: () => Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (BuildContext context) => VLPage(
                  //       this.client,
                  //       agency,
                  //       route,
                  //       vl,
                  //       vl.target.tag,
                  //     ),
                  //   ),
                  // ),
                );
              }),
            LicensingTile(snapshot.data!.licensing),
          ],
        );
      },
    );
  }
}
