import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/blocks/licensing_tile.dart';
import 'package:chokutsuflutter/pages/vl_page.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class SelectVehicle extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;

  const SelectVehicle(this.client, this.agency, this.route, {Key? key})
      : super(key: key);

  @override
  _SelectVehicleState createState() =>
      _SelectVehicleState(this.client, this.agency, this.route);
}

class _SelectVehicleState extends State<SelectVehicle> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;

  _SelectVehicleState(this.client, this.agency, this.route);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.locationsRoute(grpc.LocationsRouteReq(
        agencyTag: agency.tag,
        routeTag: route.tag,
        t: "1960-01-01T00:00:00Z",
      )),
      builder:
          (BuildContext context, AsyncSnapshot<grpc.LocationsResp> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }

        return ListView(
          children: [
            for (grpc.VehicleLocation vl in snapshot.data!.locations)
              Builder(builder: (BuildContext context) {
                grpc.Direction? dir;
                for (final dirCand in route.directions) {
                  if (dirCand.tag == vl.target.dirTag) {
                    dir = dirCand;
                  }
                }
                return ListTile(
                  title: Text(dir == null
                      ? "${vl.target.dirTag}"
                      : "${dir.branch} ${dir.name}"),
                  subtitle: Text(
                      "${vl.speed} km/h\n${DateTime.now().subtract(Duration(seconds: vl.secsSinceReport))}"),
                  trailing: Text(
                    "v${vl.target.tag}",
                    style: TextStyle(fontFamily: "monospace"),
                  ),
                  isThreeLine: true,
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => VLPage(
                        this.client,
                        agency,
                        route,
                        vl,
                        vl.target.tag,
                      ),
                    ),
                  ),
                );
              }),
            LicensingTile(snapshot.data!.licensing),
          ],
        );
      },
    );
  }
}
