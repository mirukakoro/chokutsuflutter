import 'package:chokutsuflutter/blocks/cptdb_tile.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class RView extends StatelessWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;

  const RView(this.client, this.agency, this.route,  {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        CPTDBTile("${agency.title} route ${route.tag} ${route.title}"),
        ListTile(
            title: Text(S.of(context).tag), trailing: Text(route.tag)),
        ListTile(
          title: Text(S.of(context).agency),
          trailing: Text(agency.short == "" ? agency.title : agency.short),
        ),
      ],
    );
  }
}
