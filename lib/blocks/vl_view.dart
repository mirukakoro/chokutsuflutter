import 'package:chokutsuflutter/blocks/cptdb_tile.dart';
import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:chokutsuflutter/pages/vl_page.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class VlView extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.VehicleLocation vlInit;
  final String vehicleTag;

  const VlView(
      this.client, this.agency, this.route, this.vlInit, this.vehicleTag,
      {Key? key})
      : super(key: key);

  @override
  _VlViewState createState() => _VlViewState(
      this.client, this.agency, this.route, this.vlInit, this.vehicleTag);
}

class _VlViewState extends State<VlView> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.VehicleLocation vlInit;
  final String vehicleTag;

  _VlViewState(
      this.client, this.agency, this.route, this.vlInit, this.vehicleTag);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.locations(
          grpc.LocationsReq(agencyTag: agency.tag, vehicleTags: [vehicleTag])),
      builder:
          (BuildContext context, AsyncSnapshot<grpc.LocationsResp> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return ErrorView(snapshot.error.toString());
        }
        if (!snapshot.hasData || snapshot.data!.locations.length == 0) {
          return ErrorNodataView();
        }

        grpc.VehicleLocation vl = snapshot.data!.locations[0];

        return ListView(
          children: [
            CPTDBTile("${agency.title} vehicle ${vl.target.tag}"),
            ListTile(
                title: Text(S.of(context).tag), trailing: Text(vl.target.tag)),
            ListTile(
              title: Text(S.of(context).agency),
              trailing: Text(agency.short == "" ? agency.title : agency.short),
            ),
            ListTile(
                title: Text(S.of(context).route), trailing: Text(route.title)),
            ListTile(
              title: Text(S.of(context).dirTag),
              trailing: Text(vl.target.dirTag),
            ),
            ListTile(
              title: Text(S.of(context).vehicleUpdated),
              trailing: Text(DateTime.now()
                  .subtract(Duration(seconds: vl.secsSinceReport))
                  .toString()),
            ),
            ListTile(
                title: Text(S.of(context).vehicleSpeed),
                trailing: Text("${vl.speed} km/h")),
            ListTile(
                title: Text(S.of(context).vehicleHeading),
                trailing: Text("${vl.heading}°")),
            vl.leadingTag == ""
                ? ListTile(
                    title: Text(S.of(context).vehicleLeading),
                    trailing: Text(S.of(context).none),
                  )
                : FutureBuilder(
                    future: client.locations(
                      grpc.LocationsReq(
                        agencyTag: agency.tag,
                        vehicleTags: [vl.leadingTag],
                      ),
                    ),
                    builder: (BuildContext context,
                        AsyncSnapshot<grpc.LocationsResp> snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return ListTile(
                          title: Text(S.of(context).vehicleLeading),
                          trailing: CircularProgressIndicator(),
                        );
                      }
                      if (snapshot.hasError) {
                        return ListTile(
                          title: Text(S.of(context).vehicleLeading),
                          trailing: Text(snapshot.error.toString()),
                        );
                      }
                      if (snapshot.data!.locations.length == 0) {
                        return ListTile(
                          title: Text(S.of(context).vehicleLeading),
                          trailing: Text(S.of(context).none),
                        );
                      }

                      final vl = snapshot.data!.locations[0];
                      return ListTile(
                        title: Text(S.of(context).vehicleLeading),
                        trailing: Text(vl.target.tag),
                        onTap: () async {
                          final resp = await client.locations(
                            grpc.LocationsReq(
                              agencyTag: agency.tag,
                              vehicleTags: [vl.leadingTag],
                            ),
                          );

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => VLPage(
                                this.client,
                                agency,
                                route,
                                resp.locations[0],
                                vl.leadingTag,
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
            ListTile(
              title: Text(S.of(context).vehiclePredictable),
              trailing:
                  Text(vl.predictable ? S.of(context).yes : S.of(context).no),
            ),
            ListTile(
              title: Text(S.of(context).location),
              trailing: Text(vl.location.toString()),
            ),
          ],
        );
      },
    );
  }
}
