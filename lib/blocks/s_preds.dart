import 'dart:async';

import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/blocks/licensing_tile.dart';
import 'package:chokutsuflutter/pages/vl_page.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class SPreds extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.Stop sInit;
  final grpc.Vehicle? vOnly;

  const SPreds(this.client, this.agency, this.route, this.sInit,
      {this.vOnly, Key? key})
      : super(key: key);

  @override
  _SPredsState createState() =>
      _SPredsState(this.client, this.agency, this.route, this.sInit,
          vOnly: this.vOnly);
}

class _SPredsState extends State<SPreds> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.Stop sInit;
  final grpc.Vehicle? vOnly;

  _SPredsState(this.client, this.agency, this.route, this.sInit, {this.vOnly});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client
          .predictions(grpc.PredictionsReq(agencyTag: agency.tag, rsTags: [
        grpc.RouteStopTag(routeTag: route.tag, stopTag: sInit.tag),
      ])),
      builder:
          (BuildContext context, AsyncSnapshot<grpc.PredictionsResp> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return ErrorView(snapshot.error.toString());
        }

        if (!snapshot.hasData) {
          return ErrorNodataView();
        }

        return ListView(
          children: [
            for (final pred in snapshot.data!.predictions)
              ListTile(
                title: Text(
                    DateTime.fromMillisecondsSinceEpoch(pred.epoch.toInt())
                        .difference(DateTime.now())
                        .toString()),
                enabled: vOnly == null
                    ? true
                    : vOnly!.tag == pred.v.tag
                        ? true
                        : false,
                leading: pred.affectedByLayover
                    ? Icon(Icons.warning)
                    : Icon(Icons.check),
                trailing: Text(
                  "t${pred.tripTag}",
                  style: TextStyle(fontFamily: "monospace"),
                ),
                onTap: vOnly != null && vOnly!.tag == pred.v.tag
                    ? () => Navigator.pop(context)
                    : () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) {
                              return VLPage(
                                this.client,
                                agency,
                                route,
                                grpc.VehicleLocation(
                                  target: pred.v,
                                ),
                                pred.v.tag,
                              );
                            },
                          ),
                        ),
              ),
            LicensingTile(snapshot.data!.licensing),
          ],
        );
      },
    );
  }
}
