import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/blocks/licensing_tile.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class SelectDirection extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;

  const SelectDirection(this.client, this.agency, this.route, {Key? key})
      : super(key: key);

  @override
  _SelectDirectionState createState() =>
      _SelectDirectionState(this.client, this.agency, this.route);
}

class _SelectDirectionState extends State<SelectDirection> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;

  _SelectDirectionState(this.client, this.agency, this.route);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.route(grpc.RouteReq(
        agencyTag: agency.tag,
        routeTag: route.tag,
      )),
      builder: (BuildContext context, AsyncSnapshot<grpc.RouteResp> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return ErrorView(snapshot.error.toString());
        }
        if (!snapshot.hasData) {
          return ErrorNodataView();
        }

        return ListView(
          children: [
            for (grpc.Direction direction in snapshot.data!.route.directions)
              Builder(
                builder: (BuildContext context) {
                  grpc.Stop? lastStop;
                  grpc.Stop? firstStop;
                  for (final stop in snapshot.data!.route.stops) {
                    if (stop.tag == direction.stops.first.tag) {
                      firstStop = stop;
                    }
                    if (stop.tag == direction.stops.last.tag) {
                      lastStop = stop;
                    }
                  }
                  if (firstStop == null) {
                    return ErrorView("first stop not found");
                  }
                  if (lastStop == null) {
                    return ErrorView("last stop not found");
                  }
                  return ListTile(
                    subtitle: Text(
                      "from ${firstStop.title}\nto ${lastStop.title}",
                    ),
                    title: Text("${direction.name} ${direction.branch}"),
                    trailing: Text(
                      "d${direction.tag}",
                      style: TextStyle(fontFamily: "monospace"),
                    ),
                    isThreeLine: true,
                    // onTap: () => Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (BuildContext context) =>SPage(
                    //       this.client,
                    //       agency,
                    //       route,
                    //       stop,
                    //     ),
                    //   ),
                    // ),
                  );
                },
              ),
            LicensingTile(snapshot.data!.licensing),
          ],
        );
      },
    );
  }
}
