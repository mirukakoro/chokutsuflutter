import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:chokutsuflutter/util/util.dart';
import 'package:flutter/material.dart';

class CPTDBTile extends StatelessWidget {
  final String term;

  const CPTDBTile(this.term, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(S.of(context).cptdbWiki),
      onTap: () => launchCptdb(term),
    );
  }
}
