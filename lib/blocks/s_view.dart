import 'package:chokutsuflutter/blocks/cptdb_tile.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class SView extends StatelessWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.Stop sInit;

  const SView(this.client, this.agency, this.route, this.sInit, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        CPTDBTile("${agency.title} stops ${sInit.id}"),
        ListTile(
            title: Text(S.of(context).tag), trailing: Text(sInit.tag)),
        ListTile(
            title: Text(S.of(context).stopId), trailing: Text(sInit.id)),
        ListTile(
          title: Text(S.of(context).agency),
          trailing: Text(agency.short == "" ? agency.title : agency.short),
        ),
        ListTile(
            title: Text(S.of(context).location), trailing: Text(sInit.pos.toString())),
      ],
    );
  }
}
