import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:flutter/material.dart';

class ErrorNodataView extends StatelessWidget {
  const ErrorNodataView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Icon(Icons.error),
          Text(S.of(context).errNoData),
        ],
      ),
    );
  }
}
