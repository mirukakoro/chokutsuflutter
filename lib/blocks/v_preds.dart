import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/generated/l10n.dart';
import 'package:chokutsuflutter/pages/s_page.dart';
import 'package:flutter/material.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart' as grpc;

class VPreds extends StatefulWidget {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.Vehicle vInit;

  const VPreds(this.client, this.agency, this.route, this.vInit, {Key? key})
      : super(key: key);

  @override
  _VPredsState createState() =>
      _VPredsState(this.client, this.agency, this.route, this.vInit);
}

class _VPredsState extends State<VPreds> {
  final grpc.DataClient client;
  final grpc.Agency agency;
  final grpc.Route route;
  final grpc.Vehicle vInit;

  _VPredsState(this.client, this.agency, this.route, this.vInit);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.route(grpc.RouteReq(
        agencyTag: agency.tag,
        routeTag: route.tag,
      )),
      builder: (BuildContext context, AsyncSnapshot<grpc.RouteResp> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return ErrorView(snapshot.error.toString());
        }

        if (!snapshot.hasData) {
          return ErrorNodataView();
        }

        return ListView(
          children: [
            for (final stop in snapshot.data!.route.stops)
              ListTile(
                title: Text(stop.title),
                subtitle: Text("${S.of(context).stopId} ${stop.id}"),
                trailing: Text(
                  "s${stop.tag}",
                  style: TextStyle(fontFamily: "monospace"),
                ),
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => SPage(
                      this.client,
                      agency,
                      route,
                      stop,
                      initialIndex: 2, // preds
                      vOnly: vInit,
                    ),
                  ),
                ),
              ),
          ],
        );
      },
    );
  }
}
