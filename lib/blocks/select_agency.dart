import 'package:chokutsuflutter/blocks/error_nodata_view.dart';
import 'package:chokutsuflutter/blocks/error_view.dart';
import 'package:chokutsuflutter/blocks/licensing_tile.dart';
import 'package:chokutsuflutter/pages/a_page.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart';
import 'package:flutter/material.dart';

class SelectAgency extends StatefulWidget {
  final DataClient client;

  const SelectAgency(this.client, {Key? key}) : super(key: key);

  @override
  _SelectAgencyState createState() => _SelectAgencyState(client);
}

class _SelectAgencyState extends State<SelectAgency> {
  final DataClient client;

  _SelectAgencyState(this.client);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: client.agencies(Nothing()),
      builder: (BuildContext context, AsyncSnapshot<AgenciesResp> snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.hasError) {
          return ErrorView(snapshot.error.toString());
        }
        if (!snapshot.hasData) {
          return ErrorNodataView();
        }
        return  ListView(
          children: [
              for (Agency agency in snapshot.data!.agencies)
                ListTile(
                  title: Text(agency.short == "" ? agency.title : agency.short),
                  subtitle: Text("${agency.region} ${agency.title}"),
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              APage(this.client, agency))),
                ),
              LicensingTile(snapshot.data!.licensing),
            ],

        );
      },
    );
  }
}
