import 'package:flutter/material.dart';
import '../generated/l10n.dart';

class LicensingTile extends StatelessWidget {
  final String licensing;

  const LicensingTile(this.licensing) : super();

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(S.of(context).licensingTileLicensing),
      subtitle: Text(this.licensing),
    );
  }
}
