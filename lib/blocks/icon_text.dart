import 'package:flutter/material.dart';

class Thing extends StatelessWidget {
  final IconData icon;
  final String tag;
  final String title;

  const Thing(this.icon, this.tag, this.title, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Text(
        tag,
        style: TextStyle(
          fontFamily: "monospace",
        ),
      ),
      SizedBox(width: 8),
      Icon(icon),
      SizedBox(width: 8),
      Expanded(
        child: Text(
          title,
          overflow: TextOverflow.fade,
        ),
      ),
    ]);
  }
}
