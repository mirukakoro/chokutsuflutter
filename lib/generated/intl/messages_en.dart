// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(host, port, name, version) =>
      "${name} ${version} (${host}:${port})";

  static String m1(speed, updated) => "${speed} km/h ${updated}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "aboutAPIData": m0,
        "aboutBackendTitle":
            MessageLookupByLibrary.simpleMessage("Backend Server"),
        "aboutTitle": MessageLookupByLibrary.simpleMessage("About"),
        "aboutUnknown": MessageLookupByLibrary.simpleMessage("unknown"),
        "agency": MessageLookupByLibrary.simpleMessage("Agency"),
        "appTitle": MessageLookupByLibrary.simpleMessage("Chokutsu"),
        "cptdbWiki": MessageLookupByLibrary.simpleMessage("CPTDB Wiki"),
        "dirTag": MessageLookupByLibrary.simpleMessage("Direction Tag"),
        "errNoData": MessageLookupByLibrary.simpleMessage("No data"),
        "licensingTileLicensing":
            MessageLookupByLibrary.simpleMessage("Licensing"),
        "location": MessageLookupByLibrary.simpleMessage("Location"),
        "no": MessageLookupByLibrary.simpleMessage("No"),
        "none": MessageLookupByLibrary.simpleMessage("None"),
        "refresh": MessageLookupByLibrary.simpleMessage("Refresh"),
        "route": MessageLookupByLibrary.simpleMessage("Route"),
        "selectVehicleVLSubtitle": m1,
        "stopId": MessageLookupByLibrary.simpleMessage("SMS-only Stop ID"),
        "tag": MessageLookupByLibrary.simpleMessage("Tag"),
        "vehicleHeading": MessageLookupByLibrary.simpleMessage("Heading"),
        "vehicleLeading":
            MessageLookupByLibrary.simpleMessage("Leading Vehicle"),
        "vehiclePredictable":
            MessageLookupByLibrary.simpleMessage("Predictable"),
        "vehicleSpeed": MessageLookupByLibrary.simpleMessage("Speed"),
        "vehicleUpdated": MessageLookupByLibrary.simpleMessage("Last Updated"),
        "yes": MessageLookupByLibrary.simpleMessage("Yes")
      };
}
