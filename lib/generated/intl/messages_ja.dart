// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ja locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ja';

  static String m0(host, port, name, version) =>
      "名前：${name} バージョン：${version} (${host}:${port})";

  static String m1(speed, updated) => "${speed} km/h ${updated}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "aboutAPIData": m0,
        "aboutBackendTitle": MessageLookupByLibrary.simpleMessage("バックエンドサーバ"),
        "aboutTitle": MessageLookupByLibrary.simpleMessage("アプリについて"),
        "aboutUnknown": MessageLookupByLibrary.simpleMessage("不明"),
        "agency": MessageLookupByLibrary.simpleMessage("交通機関"),
        "appTitle": MessageLookupByLibrary.simpleMessage("直通"),
        "cptdbWiki": MessageLookupByLibrary.simpleMessage("CPTDBウィキ"),
        "dirTag": MessageLookupByLibrary.simpleMessage("方向識別子"),
        "errNoData": MessageLookupByLibrary.simpleMessage("No data"),
        "licensingTileLicensing": MessageLookupByLibrary.simpleMessage("ライセンス"),
        "location": MessageLookupByLibrary.simpleMessage("位置"),
        "no": MessageLookupByLibrary.simpleMessage("いいえ"),
        "none": MessageLookupByLibrary.simpleMessage("無し"),
        "refresh": MessageLookupByLibrary.simpleMessage("再読込"),
        "route": MessageLookupByLibrary.simpleMessage("路線"),
        "selectVehicleVLSubtitle": m1,
        "stopId": MessageLookupByLibrary.simpleMessage("SMS用識別子"),
        "tag": MessageLookupByLibrary.simpleMessage("識別子"),
        "vehicleHeading": MessageLookupByLibrary.simpleMessage("方向"),
        "vehicleLeading": MessageLookupByLibrary.simpleMessage("先行車"),
        "vehiclePredictable": MessageLookupByLibrary.simpleMessage("予測可能"),
        "vehicleSpeed": MessageLookupByLibrary.simpleMessage("速度"),
        "vehicleUpdated": MessageLookupByLibrary.simpleMessage("最終更新"),
        "yes": MessageLookupByLibrary.simpleMessage("はい")
      };
}
