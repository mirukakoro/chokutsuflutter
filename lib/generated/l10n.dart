// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Chokutsu`
  String get appTitle {
    return Intl.message(
      'Chokutsu',
      name: 'appTitle',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get aboutTitle {
    return Intl.message(
      'About',
      name: 'aboutTitle',
      desc: '',
      args: [],
    );
  }

  /// `{name} {version} ({host}:{port})`
  String aboutAPIData(Object host, Object port, Object name, Object version) {
    return Intl.message(
      '$name $version ($host:$port)',
      name: 'aboutAPIData',
      desc: '',
      args: [host, port, name, version],
    );
  }

  /// `Backend Server`
  String get aboutBackendTitle {
    return Intl.message(
      'Backend Server',
      name: 'aboutBackendTitle',
      desc: '',
      args: [],
    );
  }

  /// `unknown`
  String get aboutUnknown {
    return Intl.message(
      'unknown',
      name: 'aboutUnknown',
      desc: '',
      args: [],
    );
  }

  /// `Licensing`
  String get licensingTileLicensing {
    return Intl.message(
      'Licensing',
      name: 'licensingTileLicensing',
      desc: '',
      args: [],
    );
  }

  /// `Refresh`
  String get refresh {
    return Intl.message(
      'Refresh',
      name: 'refresh',
      desc: '',
      args: [],
    );
  }

  /// `{speed} km/h {updated}`
  String selectVehicleVLSubtitle(Object speed, Object updated) {
    return Intl.message(
      '$speed km/h $updated',
      name: 'selectVehicleVLSubtitle',
      desc: '',
      args: [speed, updated],
    );
  }

  /// `Location`
  String get location {
    return Intl.message(
      'Location',
      name: 'location',
      desc: '',
      args: [],
    );
  }

  /// `Predictable`
  String get vehiclePredictable {
    return Intl.message(
      'Predictable',
      name: 'vehiclePredictable',
      desc: '',
      args: [],
    );
  }

  /// `Leading Vehicle`
  String get vehicleLeading {
    return Intl.message(
      'Leading Vehicle',
      name: 'vehicleLeading',
      desc: '',
      args: [],
    );
  }

  /// `Speed`
  String get vehicleSpeed {
    return Intl.message(
      'Speed',
      name: 'vehicleSpeed',
      desc: '',
      args: [],
    );
  }

  /// `Heading`
  String get vehicleHeading {
    return Intl.message(
      'Heading',
      name: 'vehicleHeading',
      desc: '',
      args: [],
    );
  }

  /// `Last Updated`
  String get vehicleUpdated {
    return Intl.message(
      'Last Updated',
      name: 'vehicleUpdated',
      desc: '',
      args: [],
    );
  }

  /// `Direction Tag`
  String get dirTag {
    return Intl.message(
      'Direction Tag',
      name: 'dirTag',
      desc: '',
      args: [],
    );
  }

  /// `Tag`
  String get tag {
    return Intl.message(
      'Tag',
      name: 'tag',
      desc: '',
      args: [],
    );
  }

  /// `Route`
  String get route {
    return Intl.message(
      'Route',
      name: 'route',
      desc: '',
      args: [],
    );
  }

  /// `Agency`
  String get agency {
    return Intl.message(
      'Agency',
      name: 'agency',
      desc: '',
      args: [],
    );
  }

  /// `CPTDB Wiki`
  String get cptdbWiki {
    return Intl.message(
      'CPTDB Wiki',
      name: 'cptdbWiki',
      desc: '',
      args: [],
    );
  }

  /// `None`
  String get none {
    return Intl.message(
      'None',
      name: 'none',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `No data`
  String get errNoData {
    return Intl.message(
      'No data',
      name: 'errNoData',
      desc: '',
      args: [],
    );
  }

  /// `SMS-only Stop ID`
  String get stopId {
    return Intl.message(
      'SMS-only Stop ID',
      name: 'stopId',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ja'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
