import 'package:chokutsuflutter/main.dart';
import 'package:chokutsuflutter/proto/main.pbgrpc.dart';
import 'package:flutter/material.dart';
import 'blocks/error_nodata_view.dart';
import 'blocks/error_view.dart';
import 'generated/l10n.dart';

class AboutPage extends StatefulWidget {
  final DataClient client;

  const AboutPage(this.client) : super();

  static const id = '/about';

  @override
  _AboutPageState createState() => _AboutPageState(client);
}

class _AboutPageState extends State<AboutPage> {
  final DataClient client;

  _AboutPageState(this.client);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).aboutTitle),
      ),
      body: ListView(
        children: [
          AboutListTile(
            applicationName: S.of(context).appTitle,
            applicationVersion: "(dev)",
          ),
          ListTile(
            title: Text(S.of(context).aboutBackendTitle),
            subtitle: FutureBuilder(
              future: client.name(Nothing()),
              builder:
                  (BuildContext context, AsyncSnapshot<NameResp> snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return LinearProgressIndicator();
                }
                if (snapshot.hasError) {
                  return ErrorView(snapshot.error.toString());
                }
                if (!snapshot.hasData) {
                  return ErrorNodataView();
                }
                return Text(S.of(context).aboutAPIData(
                      channelHost,
                      channelPort,
                      snapshot.data?.name == null
                          ? S.of(context).aboutUnknown
                          : snapshot.data!.name,
                      snapshot.data?.version == null
                          ? S.of(context).aboutUnknown
                          : snapshot.data!.version,
                    ));
              },
            ),
          ),
        ],
      ),
    );
  }
}
