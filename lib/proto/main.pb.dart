///
//  Generated code. Do not modify.
//  source: main.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

class Nothing extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Nothing', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Nothing._() : super();
  factory Nothing() => create();
  factory Nothing.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Nothing.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Nothing clone() => Nothing()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Nothing copyWith(void Function(Nothing) updates) => super.copyWith((message) => updates(message as Nothing)) as Nothing; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Nothing create() => Nothing._();
  Nothing createEmptyInstance() => create();
  static $pb.PbList<Nothing> createRepeated() => $pb.PbList<Nothing>();
  @$core.pragma('dart2js:noInline')
  static Nothing getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Nothing>(create);
  static Nothing? _defaultInstance;
}

class NameResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NameResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version')
    ..hasRequiredFields = false
  ;

  NameResp._() : super();
  factory NameResp({
    $core.String? name,
    $core.String? version,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (version != null) {
      _result.version = version;
    }
    return _result;
  }
  factory NameResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NameResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NameResp clone() => NameResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NameResp copyWith(void Function(NameResp) updates) => super.copyWith((message) => updates(message as NameResp)) as NameResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NameResp create() => NameResp._();
  NameResp createEmptyInstance() => create();
  static $pb.PbList<NameResp> createRepeated() => $pb.PbList<NameResp>();
  @$core.pragma('dart2js:noInline')
  static NameResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NameResp>(create);
  static NameResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get version => $_getSZ(1);
  @$pb.TagNumber(2)
  set version($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVersion() => $_has(1);
  @$pb.TagNumber(2)
  void clearVersion() => clearField(2);
}

class AgenciesResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AgenciesResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'licensing')
    ..pc<Agency>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agencies', $pb.PbFieldType.PM, subBuilder: Agency.create)
    ..hasRequiredFields = false
  ;

  AgenciesResp._() : super();
  factory AgenciesResp({
    $core.String? licensing,
    $core.Iterable<Agency>? agencies,
  }) {
    final _result = create();
    if (licensing != null) {
      _result.licensing = licensing;
    }
    if (agencies != null) {
      _result.agencies.addAll(agencies);
    }
    return _result;
  }
  factory AgenciesResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AgenciesResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AgenciesResp clone() => AgenciesResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AgenciesResp copyWith(void Function(AgenciesResp) updates) => super.copyWith((message) => updates(message as AgenciesResp)) as AgenciesResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AgenciesResp create() => AgenciesResp._();
  AgenciesResp createEmptyInstance() => create();
  static $pb.PbList<AgenciesResp> createRepeated() => $pb.PbList<AgenciesResp>();
  @$core.pragma('dart2js:noInline')
  static AgenciesResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AgenciesResp>(create);
  static AgenciesResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get licensing => $_getSZ(0);
  @$pb.TagNumber(1)
  set licensing($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLicensing() => $_has(0);
  @$pb.TagNumber(1)
  void clearLicensing() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Agency> get agencies => $_getList(1);
}

class RoutesReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RoutesReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agencyTag', protoName: 'agencyTag')
    ..hasRequiredFields = false
  ;

  RoutesReq._() : super();
  factory RoutesReq({
    $core.String? agencyTag,
  }) {
    final _result = create();
    if (agencyTag != null) {
      _result.agencyTag = agencyTag;
    }
    return _result;
  }
  factory RoutesReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RoutesReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RoutesReq clone() => RoutesReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RoutesReq copyWith(void Function(RoutesReq) updates) => super.copyWith((message) => updates(message as RoutesReq)) as RoutesReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RoutesReq create() => RoutesReq._();
  RoutesReq createEmptyInstance() => create();
  static $pb.PbList<RoutesReq> createRepeated() => $pb.PbList<RoutesReq>();
  @$core.pragma('dart2js:noInline')
  static RoutesReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RoutesReq>(create);
  static RoutesReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get agencyTag => $_getSZ(0);
  @$pb.TagNumber(1)
  set agencyTag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAgencyTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearAgencyTag() => clearField(1);
}

class RoutesResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RoutesResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'licensing')
    ..pc<Route>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'routes', $pb.PbFieldType.PM, subBuilder: Route.create)
    ..hasRequiredFields = false
  ;

  RoutesResp._() : super();
  factory RoutesResp({
    $core.String? licensing,
    $core.Iterable<Route>? routes,
  }) {
    final _result = create();
    if (licensing != null) {
      _result.licensing = licensing;
    }
    if (routes != null) {
      _result.routes.addAll(routes);
    }
    return _result;
  }
  factory RoutesResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RoutesResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RoutesResp clone() => RoutesResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RoutesResp copyWith(void Function(RoutesResp) updates) => super.copyWith((message) => updates(message as RoutesResp)) as RoutesResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RoutesResp create() => RoutesResp._();
  RoutesResp createEmptyInstance() => create();
  static $pb.PbList<RoutesResp> createRepeated() => $pb.PbList<RoutesResp>();
  @$core.pragma('dart2js:noInline')
  static RoutesResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RoutesResp>(create);
  static RoutesResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get licensing => $_getSZ(0);
  @$pb.TagNumber(1)
  set licensing($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLicensing() => $_has(0);
  @$pb.TagNumber(1)
  void clearLicensing() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Route> get routes => $_getList(1);
}

class RouteReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RouteReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agencyTag', protoName: 'agencyTag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'routeTag', protoName: 'routeTag')
    ..hasRequiredFields = false
  ;

  RouteReq._() : super();
  factory RouteReq({
    $core.String? agencyTag,
    $core.String? routeTag,
  }) {
    final _result = create();
    if (agencyTag != null) {
      _result.agencyTag = agencyTag;
    }
    if (routeTag != null) {
      _result.routeTag = routeTag;
    }
    return _result;
  }
  factory RouteReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RouteReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RouteReq clone() => RouteReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RouteReq copyWith(void Function(RouteReq) updates) => super.copyWith((message) => updates(message as RouteReq)) as RouteReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RouteReq create() => RouteReq._();
  RouteReq createEmptyInstance() => create();
  static $pb.PbList<RouteReq> createRepeated() => $pb.PbList<RouteReq>();
  @$core.pragma('dart2js:noInline')
  static RouteReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RouteReq>(create);
  static RouteReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get agencyTag => $_getSZ(0);
  @$pb.TagNumber(1)
  set agencyTag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAgencyTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearAgencyTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get routeTag => $_getSZ(1);
  @$pb.TagNumber(2)
  set routeTag($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRouteTag() => $_has(1);
  @$pb.TagNumber(2)
  void clearRouteTag() => clearField(2);
}

class RouteResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RouteResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'licensing')
    ..aOM<Route>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'route', subBuilder: Route.create)
    ..hasRequiredFields = false
  ;

  RouteResp._() : super();
  factory RouteResp({
    $core.String? licensing,
    Route? route,
  }) {
    final _result = create();
    if (licensing != null) {
      _result.licensing = licensing;
    }
    if (route != null) {
      _result.route = route;
    }
    return _result;
  }
  factory RouteResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RouteResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RouteResp clone() => RouteResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RouteResp copyWith(void Function(RouteResp) updates) => super.copyWith((message) => updates(message as RouteResp)) as RouteResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RouteResp create() => RouteResp._();
  RouteResp createEmptyInstance() => create();
  static $pb.PbList<RouteResp> createRepeated() => $pb.PbList<RouteResp>();
  @$core.pragma('dart2js:noInline')
  static RouteResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RouteResp>(create);
  static RouteResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get licensing => $_getSZ(0);
  @$pb.TagNumber(1)
  set licensing($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLicensing() => $_has(0);
  @$pb.TagNumber(1)
  void clearLicensing() => clearField(1);

  @$pb.TagNumber(2)
  Route get route => $_getN(1);
  @$pb.TagNumber(2)
  set route(Route v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasRoute() => $_has(1);
  @$pb.TagNumber(2)
  void clearRoute() => clearField(2);
  @$pb.TagNumber(2)
  Route ensureRoute() => $_ensure(1);
}

class LocationsRouteReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LocationsRouteReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agencyTag', protoName: 'agencyTag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'routeTag', protoName: 'routeTag')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 't')
    ..hasRequiredFields = false
  ;

  LocationsRouteReq._() : super();
  factory LocationsRouteReq({
    $core.String? agencyTag,
    $core.String? routeTag,
    $core.String? t,
  }) {
    final _result = create();
    if (agencyTag != null) {
      _result.agencyTag = agencyTag;
    }
    if (routeTag != null) {
      _result.routeTag = routeTag;
    }
    if (t != null) {
      _result.t = t;
    }
    return _result;
  }
  factory LocationsRouteReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LocationsRouteReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LocationsRouteReq clone() => LocationsRouteReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LocationsRouteReq copyWith(void Function(LocationsRouteReq) updates) => super.copyWith((message) => updates(message as LocationsRouteReq)) as LocationsRouteReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LocationsRouteReq create() => LocationsRouteReq._();
  LocationsRouteReq createEmptyInstance() => create();
  static $pb.PbList<LocationsRouteReq> createRepeated() => $pb.PbList<LocationsRouteReq>();
  @$core.pragma('dart2js:noInline')
  static LocationsRouteReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LocationsRouteReq>(create);
  static LocationsRouteReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get agencyTag => $_getSZ(0);
  @$pb.TagNumber(1)
  set agencyTag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAgencyTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearAgencyTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get routeTag => $_getSZ(1);
  @$pb.TagNumber(2)
  set routeTag($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRouteTag() => $_has(1);
  @$pb.TagNumber(2)
  void clearRouteTag() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get t => $_getSZ(2);
  @$pb.TagNumber(3)
  set t($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasT() => $_has(2);
  @$pb.TagNumber(3)
  void clearT() => clearField(3);
}

class LocationsReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LocationsReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agencyTag', protoName: 'agencyTag')
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'vehicleTags', protoName: 'vehicleTags')
    ..hasRequiredFields = false
  ;

  LocationsReq._() : super();
  factory LocationsReq({
    $core.String? agencyTag,
    $core.Iterable<$core.String>? vehicleTags,
  }) {
    final _result = create();
    if (agencyTag != null) {
      _result.agencyTag = agencyTag;
    }
    if (vehicleTags != null) {
      _result.vehicleTags.addAll(vehicleTags);
    }
    return _result;
  }
  factory LocationsReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LocationsReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LocationsReq clone() => LocationsReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LocationsReq copyWith(void Function(LocationsReq) updates) => super.copyWith((message) => updates(message as LocationsReq)) as LocationsReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LocationsReq create() => LocationsReq._();
  LocationsReq createEmptyInstance() => create();
  static $pb.PbList<LocationsReq> createRepeated() => $pb.PbList<LocationsReq>();
  @$core.pragma('dart2js:noInline')
  static LocationsReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LocationsReq>(create);
  static LocationsReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get agencyTag => $_getSZ(0);
  @$pb.TagNumber(1)
  set agencyTag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAgencyTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearAgencyTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get vehicleTags => $_getList(1);
}

class LocationsResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LocationsResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'licensing')
    ..pc<VehicleLocation>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'locations', $pb.PbFieldType.PM, subBuilder: VehicleLocation.create)
    ..hasRequiredFields = false
  ;

  LocationsResp._() : super();
  factory LocationsResp({
    $core.String? licensing,
    $core.Iterable<VehicleLocation>? locations,
  }) {
    final _result = create();
    if (licensing != null) {
      _result.licensing = licensing;
    }
    if (locations != null) {
      _result.locations.addAll(locations);
    }
    return _result;
  }
  factory LocationsResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LocationsResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LocationsResp clone() => LocationsResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LocationsResp copyWith(void Function(LocationsResp) updates) => super.copyWith((message) => updates(message as LocationsResp)) as LocationsResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LocationsResp create() => LocationsResp._();
  LocationsResp createEmptyInstance() => create();
  static $pb.PbList<LocationsResp> createRepeated() => $pb.PbList<LocationsResp>();
  @$core.pragma('dart2js:noInline')
  static LocationsResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LocationsResp>(create);
  static LocationsResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get licensing => $_getSZ(0);
  @$pb.TagNumber(1)
  set licensing($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLicensing() => $_has(0);
  @$pb.TagNumber(1)
  void clearLicensing() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<VehicleLocation> get locations => $_getList(1);
}

class PredictionsReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PredictionsReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agencyTag', protoName: 'agencyTag')
    ..pc<RouteStopTag>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rsTags', $pb.PbFieldType.PM, protoName: 'rsTags', subBuilder: RouteStopTag.create)
    ..hasRequiredFields = false
  ;

  PredictionsReq._() : super();
  factory PredictionsReq({
    $core.String? agencyTag,
    $core.Iterable<RouteStopTag>? rsTags,
  }) {
    final _result = create();
    if (agencyTag != null) {
      _result.agencyTag = agencyTag;
    }
    if (rsTags != null) {
      _result.rsTags.addAll(rsTags);
    }
    return _result;
  }
  factory PredictionsReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PredictionsReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PredictionsReq clone() => PredictionsReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PredictionsReq copyWith(void Function(PredictionsReq) updates) => super.copyWith((message) => updates(message as PredictionsReq)) as PredictionsReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PredictionsReq create() => PredictionsReq._();
  PredictionsReq createEmptyInstance() => create();
  static $pb.PbList<PredictionsReq> createRepeated() => $pb.PbList<PredictionsReq>();
  @$core.pragma('dart2js:noInline')
  static PredictionsReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PredictionsReq>(create);
  static PredictionsReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get agencyTag => $_getSZ(0);
  @$pb.TagNumber(1)
  set agencyTag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAgencyTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearAgencyTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<RouteStopTag> get rsTags => $_getList(1);
}

class PredictionsResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PredictionsResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'licensing')
    ..pc<Prediction>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'predictions', $pb.PbFieldType.PM, subBuilder: Prediction.create)
    ..hasRequiredFields = false
  ;

  PredictionsResp._() : super();
  factory PredictionsResp({
    $core.String? licensing,
    $core.Iterable<Prediction>? predictions,
  }) {
    final _result = create();
    if (licensing != null) {
      _result.licensing = licensing;
    }
    if (predictions != null) {
      _result.predictions.addAll(predictions);
    }
    return _result;
  }
  factory PredictionsResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PredictionsResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PredictionsResp clone() => PredictionsResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PredictionsResp copyWith(void Function(PredictionsResp) updates) => super.copyWith((message) => updates(message as PredictionsResp)) as PredictionsResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PredictionsResp create() => PredictionsResp._();
  PredictionsResp createEmptyInstance() => create();
  static $pb.PbList<PredictionsResp> createRepeated() => $pb.PbList<PredictionsResp>();
  @$core.pragma('dart2js:noInline')
  static PredictionsResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PredictionsResp>(create);
  static PredictionsResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get licensing => $_getSZ(0);
  @$pb.TagNumber(1)
  set licensing($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLicensing() => $_has(0);
  @$pb.TagNumber(1)
  void clearLicensing() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Prediction> get predictions => $_getList(1);
}

class MessagesReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MessagesReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'agencyTag', protoName: 'agencyTag')
    ..pPS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'routeTags', protoName: 'routeTags')
    ..hasRequiredFields = false
  ;

  MessagesReq._() : super();
  factory MessagesReq({
    $core.String? agencyTag,
    $core.Iterable<$core.String>? routeTags,
  }) {
    final _result = create();
    if (agencyTag != null) {
      _result.agencyTag = agencyTag;
    }
    if (routeTags != null) {
      _result.routeTags.addAll(routeTags);
    }
    return _result;
  }
  factory MessagesReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MessagesReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MessagesReq clone() => MessagesReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MessagesReq copyWith(void Function(MessagesReq) updates) => super.copyWith((message) => updates(message as MessagesReq)) as MessagesReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MessagesReq create() => MessagesReq._();
  MessagesReq createEmptyInstance() => create();
  static $pb.PbList<MessagesReq> createRepeated() => $pb.PbList<MessagesReq>();
  @$core.pragma('dart2js:noInline')
  static MessagesReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MessagesReq>(create);
  static MessagesReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get agencyTag => $_getSZ(0);
  @$pb.TagNumber(1)
  set agencyTag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAgencyTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearAgencyTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.String> get routeTags => $_getList(1);
}

class MessagesResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MessagesResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'licensing')
    ..pc<Message>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messages', $pb.PbFieldType.PM, subBuilder: Message.create)
    ..hasRequiredFields = false
  ;

  MessagesResp._() : super();
  factory MessagesResp({
    $core.String? licensing,
    $core.Iterable<Message>? messages,
  }) {
    final _result = create();
    if (licensing != null) {
      _result.licensing = licensing;
    }
    if (messages != null) {
      _result.messages.addAll(messages);
    }
    return _result;
  }
  factory MessagesResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MessagesResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MessagesResp clone() => MessagesResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MessagesResp copyWith(void Function(MessagesResp) updates) => super.copyWith((message) => updates(message as MessagesResp)) as MessagesResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MessagesResp create() => MessagesResp._();
  MessagesResp createEmptyInstance() => create();
  static $pb.PbList<MessagesResp> createRepeated() => $pb.PbList<MessagesResp>();
  @$core.pragma('dart2js:noInline')
  static MessagesResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MessagesResp>(create);
  static MessagesResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get licensing => $_getSZ(0);
  @$pb.TagNumber(1)
  set licensing($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLicensing() => $_has(0);
  @$pb.TagNumber(1)
  void clearLicensing() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Message> get messages => $_getList(1);
}

class Agency extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Agency', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'region')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'short')
    ..pc<Route>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'routes', $pb.PbFieldType.PM, subBuilder: Route.create)
    ..hasRequiredFields = false
  ;

  Agency._() : super();
  factory Agency({
    $core.String? tag,
    $core.String? title,
    $core.String? region,
    $core.String? short,
    $core.Iterable<Route>? routes,
  }) {
    final _result = create();
    if (tag != null) {
      _result.tag = tag;
    }
    if (title != null) {
      _result.title = title;
    }
    if (region != null) {
      _result.region = region;
    }
    if (short != null) {
      _result.short = short;
    }
    if (routes != null) {
      _result.routes.addAll(routes);
    }
    return _result;
  }
  factory Agency.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Agency.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Agency clone() => Agency()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Agency copyWith(void Function(Agency) updates) => super.copyWith((message) => updates(message as Agency)) as Agency; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Agency create() => Agency._();
  Agency createEmptyInstance() => create();
  static $pb.PbList<Agency> createRepeated() => $pb.PbList<Agency>();
  @$core.pragma('dart2js:noInline')
  static Agency getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Agency>(create);
  static Agency? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tag => $_getSZ(0);
  @$pb.TagNumber(1)
  set tag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get title => $_getSZ(1);
  @$pb.TagNumber(2)
  set title($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTitle() => $_has(1);
  @$pb.TagNumber(2)
  void clearTitle() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get region => $_getSZ(2);
  @$pb.TagNumber(3)
  set region($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRegion() => $_has(2);
  @$pb.TagNumber(3)
  void clearRegion() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get short => $_getSZ(3);
  @$pb.TagNumber(4)
  set short($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasShort() => $_has(3);
  @$pb.TagNumber(4)
  void clearShort() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<Route> get routes => $_getList(4);
}

class Route extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Route', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'color')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'colorOpposite', protoName: 'colorOpposite')
    ..aOM<Point>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'min', subBuilder: Point.create)
    ..aOM<Point>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'max', subBuilder: Point.create)
    ..pc<Stop>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stops', $pb.PbFieldType.PM, subBuilder: Stop.create)
    ..pc<Direction>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'directions', $pb.PbFieldType.PM, subBuilder: Direction.create)
    ..pc<Path>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'paths', $pb.PbFieldType.PM, subBuilder: Path.create)
    ..hasRequiredFields = false
  ;

  Route._() : super();
  factory Route({
    $core.String? tag,
    $core.String? title,
    $core.String? color,
    $core.String? colorOpposite,
    Point? min,
    Point? max,
    $core.Iterable<Stop>? stops,
    $core.Iterable<Direction>? directions,
    $core.Iterable<Path>? paths,
  }) {
    final _result = create();
    if (tag != null) {
      _result.tag = tag;
    }
    if (title != null) {
      _result.title = title;
    }
    if (color != null) {
      _result.color = color;
    }
    if (colorOpposite != null) {
      _result.colorOpposite = colorOpposite;
    }
    if (min != null) {
      _result.min = min;
    }
    if (max != null) {
      _result.max = max;
    }
    if (stops != null) {
      _result.stops.addAll(stops);
    }
    if (directions != null) {
      _result.directions.addAll(directions);
    }
    if (paths != null) {
      _result.paths.addAll(paths);
    }
    return _result;
  }
  factory Route.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Route.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Route clone() => Route()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Route copyWith(void Function(Route) updates) => super.copyWith((message) => updates(message as Route)) as Route; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Route create() => Route._();
  Route createEmptyInstance() => create();
  static $pb.PbList<Route> createRepeated() => $pb.PbList<Route>();
  @$core.pragma('dart2js:noInline')
  static Route getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Route>(create);
  static Route? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tag => $_getSZ(0);
  @$pb.TagNumber(1)
  set tag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get title => $_getSZ(1);
  @$pb.TagNumber(2)
  set title($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTitle() => $_has(1);
  @$pb.TagNumber(2)
  void clearTitle() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get color => $_getSZ(2);
  @$pb.TagNumber(3)
  set color($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasColor() => $_has(2);
  @$pb.TagNumber(3)
  void clearColor() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get colorOpposite => $_getSZ(3);
  @$pb.TagNumber(4)
  set colorOpposite($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasColorOpposite() => $_has(3);
  @$pb.TagNumber(4)
  void clearColorOpposite() => clearField(4);

  @$pb.TagNumber(5)
  Point get min => $_getN(4);
  @$pb.TagNumber(5)
  set min(Point v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasMin() => $_has(4);
  @$pb.TagNumber(5)
  void clearMin() => clearField(5);
  @$pb.TagNumber(5)
  Point ensureMin() => $_ensure(4);

  @$pb.TagNumber(6)
  Point get max => $_getN(5);
  @$pb.TagNumber(6)
  set max(Point v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasMax() => $_has(5);
  @$pb.TagNumber(6)
  void clearMax() => clearField(6);
  @$pb.TagNumber(6)
  Point ensureMax() => $_ensure(5);

  @$pb.TagNumber(7)
  $core.List<Stop> get stops => $_getList(6);

  @$pb.TagNumber(8)
  $core.List<Direction> get directions => $_getList(7);

  @$pb.TagNumber(9)
  $core.List<Path> get paths => $_getList(8);
}

class Stop extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Stop', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOM<Point>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pos', subBuilder: Point.create)
    ..hasRequiredFields = false
  ;

  Stop._() : super();
  factory Stop({
    $core.String? tag,
    $core.String? title,
    $core.String? id,
    Point? pos,
  }) {
    final _result = create();
    if (tag != null) {
      _result.tag = tag;
    }
    if (title != null) {
      _result.title = title;
    }
    if (id != null) {
      _result.id = id;
    }
    if (pos != null) {
      _result.pos = pos;
    }
    return _result;
  }
  factory Stop.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Stop.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Stop clone() => Stop()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Stop copyWith(void Function(Stop) updates) => super.copyWith((message) => updates(message as Stop)) as Stop; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Stop create() => Stop._();
  Stop createEmptyInstance() => create();
  static $pb.PbList<Stop> createRepeated() => $pb.PbList<Stop>();
  @$core.pragma('dart2js:noInline')
  static Stop getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Stop>(create);
  static Stop? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tag => $_getSZ(0);
  @$pb.TagNumber(1)
  set tag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get title => $_getSZ(1);
  @$pb.TagNumber(2)
  set title($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTitle() => $_has(1);
  @$pb.TagNumber(2)
  void clearTitle() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get id => $_getSZ(2);
  @$pb.TagNumber(3)
  set id($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasId() => $_has(2);
  @$pb.TagNumber(3)
  void clearId() => clearField(3);

  @$pb.TagNumber(4)
  Point get pos => $_getN(3);
  @$pb.TagNumber(4)
  set pos(Point v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasPos() => $_has(3);
  @$pb.TagNumber(4)
  void clearPos() => clearField(4);
  @$pb.TagNumber(4)
  Point ensurePos() => $_ensure(3);
}

class Direction extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Direction', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'branch')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'useForUI', protoName: 'useForUI')
    ..pc<Stop>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stops', $pb.PbFieldType.PM, subBuilder: Stop.create)
    ..hasRequiredFields = false
  ;

  Direction._() : super();
  factory Direction({
    $core.String? tag,
    $core.String? title,
    $core.String? name,
    $core.String? branch,
    $core.bool? useForUI,
    $core.Iterable<Stop>? stops,
  }) {
    final _result = create();
    if (tag != null) {
      _result.tag = tag;
    }
    if (title != null) {
      _result.title = title;
    }
    if (name != null) {
      _result.name = name;
    }
    if (branch != null) {
      _result.branch = branch;
    }
    if (useForUI != null) {
      _result.useForUI = useForUI;
    }
    if (stops != null) {
      _result.stops.addAll(stops);
    }
    return _result;
  }
  factory Direction.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Direction.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Direction clone() => Direction()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Direction copyWith(void Function(Direction) updates) => super.copyWith((message) => updates(message as Direction)) as Direction; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Direction create() => Direction._();
  Direction createEmptyInstance() => create();
  static $pb.PbList<Direction> createRepeated() => $pb.PbList<Direction>();
  @$core.pragma('dart2js:noInline')
  static Direction getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Direction>(create);
  static Direction? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tag => $_getSZ(0);
  @$pb.TagNumber(1)
  set tag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get title => $_getSZ(1);
  @$pb.TagNumber(2)
  set title($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTitle() => $_has(1);
  @$pb.TagNumber(2)
  void clearTitle() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get name => $_getSZ(2);
  @$pb.TagNumber(3)
  set name($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get branch => $_getSZ(3);
  @$pb.TagNumber(4)
  set branch($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasBranch() => $_has(3);
  @$pb.TagNumber(4)
  void clearBranch() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get useForUI => $_getBF(4);
  @$pb.TagNumber(5)
  set useForUI($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasUseForUI() => $_has(4);
  @$pb.TagNumber(5)
  void clearUseForUI() => clearField(5);

  @$pb.TagNumber(6)
  $core.List<Stop> get stops => $_getList(5);
}

class Path extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Path', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..pc<Point>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'points', $pb.PbFieldType.PM, subBuilder: Point.create)
    ..hasRequiredFields = false
  ;

  Path._() : super();
  factory Path({
    $core.Iterable<Point>? points,
  }) {
    final _result = create();
    if (points != null) {
      _result.points.addAll(points);
    }
    return _result;
  }
  factory Path.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Path.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Path clone() => Path()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Path copyWith(void Function(Path) updates) => super.copyWith((message) => updates(message as Path)) as Path; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Path create() => Path._();
  Path createEmptyInstance() => create();
  static $pb.PbList<Path> createRepeated() => $pb.PbList<Path>();
  @$core.pragma('dart2js:noInline')
  static Path getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Path>(create);
  static Path? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Point> get points => $_getList(0);
}

class Point extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Point', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lat', $pb.PbFieldType.OF)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lon', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  Point._() : super();
  factory Point({
    $core.double? lat,
    $core.double? lon,
  }) {
    final _result = create();
    if (lat != null) {
      _result.lat = lat;
    }
    if (lon != null) {
      _result.lon = lon;
    }
    return _result;
  }
  factory Point.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Point.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Point clone() => Point()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Point copyWith(void Function(Point) updates) => super.copyWith((message) => updates(message as Point)) as Point; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Point create() => Point._();
  Point createEmptyInstance() => create();
  static $pb.PbList<Point> createRepeated() => $pb.PbList<Point>();
  @$core.pragma('dart2js:noInline')
  static Point getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Point>(create);
  static Point? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get lat => $_getN(0);
  @$pb.TagNumber(1)
  set lat($core.double v) { $_setFloat(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLat() => $_has(0);
  @$pb.TagNumber(1)
  void clearLat() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get lon => $_getN(1);
  @$pb.TagNumber(2)
  set lon($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLon() => $_has(1);
  @$pb.TagNumber(2)
  void clearLon() => clearField(2);
}

class Vehicle extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Vehicle', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'routeTag', protoName: 'routeTag')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'dirTag', protoName: 'dirTag')
    ..hasRequiredFields = false
  ;

  Vehicle._() : super();
  factory Vehicle({
    $core.String? tag,
    $core.String? routeTag,
    $core.String? dirTag,
  }) {
    final _result = create();
    if (tag != null) {
      _result.tag = tag;
    }
    if (routeTag != null) {
      _result.routeTag = routeTag;
    }
    if (dirTag != null) {
      _result.dirTag = dirTag;
    }
    return _result;
  }
  factory Vehicle.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Vehicle.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Vehicle clone() => Vehicle()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Vehicle copyWith(void Function(Vehicle) updates) => super.copyWith((message) => updates(message as Vehicle)) as Vehicle; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Vehicle create() => Vehicle._();
  Vehicle createEmptyInstance() => create();
  static $pb.PbList<Vehicle> createRepeated() => $pb.PbList<Vehicle>();
  @$core.pragma('dart2js:noInline')
  static Vehicle getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Vehicle>(create);
  static Vehicle? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tag => $_getSZ(0);
  @$pb.TagNumber(1)
  set tag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get routeTag => $_getSZ(1);
  @$pb.TagNumber(2)
  set routeTag($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRouteTag() => $_has(1);
  @$pb.TagNumber(2)
  void clearRouteTag() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get dirTag => $_getSZ(2);
  @$pb.TagNumber(3)
  set dirTag($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDirTag() => $_has(2);
  @$pb.TagNumber(3)
  void clearDirTag() => clearField(3);
}

class VehicleLocation extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'VehicleLocation', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOM<Vehicle>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'target', subBuilder: Vehicle.create)
    ..aOM<Point>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'location', subBuilder: Point.create)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'secsSinceReport', $pb.PbFieldType.O3, protoName: 'secsSinceReport')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'predictable')
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'heading', $pb.PbFieldType.O3)
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'speed', $pb.PbFieldType.O3)
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'leadingTag', protoName: 'leadingTag')
    ..hasRequiredFields = false
  ;

  VehicleLocation._() : super();
  factory VehicleLocation({
    Vehicle? target,
    Point? location,
    $core.int? secsSinceReport,
    $core.bool? predictable,
    $core.int? heading,
    $core.int? speed,
    $core.String? leadingTag,
  }) {
    final _result = create();
    if (target != null) {
      _result.target = target;
    }
    if (location != null) {
      _result.location = location;
    }
    if (secsSinceReport != null) {
      _result.secsSinceReport = secsSinceReport;
    }
    if (predictable != null) {
      _result.predictable = predictable;
    }
    if (heading != null) {
      _result.heading = heading;
    }
    if (speed != null) {
      _result.speed = speed;
    }
    if (leadingTag != null) {
      _result.leadingTag = leadingTag;
    }
    return _result;
  }
  factory VehicleLocation.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VehicleLocation.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  VehicleLocation clone() => VehicleLocation()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  VehicleLocation copyWith(void Function(VehicleLocation) updates) => super.copyWith((message) => updates(message as VehicleLocation)) as VehicleLocation; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VehicleLocation create() => VehicleLocation._();
  VehicleLocation createEmptyInstance() => create();
  static $pb.PbList<VehicleLocation> createRepeated() => $pb.PbList<VehicleLocation>();
  @$core.pragma('dart2js:noInline')
  static VehicleLocation getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VehicleLocation>(create);
  static VehicleLocation? _defaultInstance;

  @$pb.TagNumber(1)
  Vehicle get target => $_getN(0);
  @$pb.TagNumber(1)
  set target(Vehicle v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasTarget() => $_has(0);
  @$pb.TagNumber(1)
  void clearTarget() => clearField(1);
  @$pb.TagNumber(1)
  Vehicle ensureTarget() => $_ensure(0);

  @$pb.TagNumber(2)
  Point get location => $_getN(1);
  @$pb.TagNumber(2)
  set location(Point v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasLocation() => $_has(1);
  @$pb.TagNumber(2)
  void clearLocation() => clearField(2);
  @$pb.TagNumber(2)
  Point ensureLocation() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.int get secsSinceReport => $_getIZ(2);
  @$pb.TagNumber(3)
  set secsSinceReport($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSecsSinceReport() => $_has(2);
  @$pb.TagNumber(3)
  void clearSecsSinceReport() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get predictable => $_getBF(3);
  @$pb.TagNumber(4)
  set predictable($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPredictable() => $_has(3);
  @$pb.TagNumber(4)
  void clearPredictable() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get heading => $_getIZ(4);
  @$pb.TagNumber(5)
  set heading($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasHeading() => $_has(4);
  @$pb.TagNumber(5)
  void clearHeading() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get speed => $_getIZ(5);
  @$pb.TagNumber(6)
  set speed($core.int v) { $_setSignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasSpeed() => $_has(5);
  @$pb.TagNumber(6)
  void clearSpeed() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get leadingTag => $_getSZ(6);
  @$pb.TagNumber(7)
  set leadingTag($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasLeadingTag() => $_has(6);
  @$pb.TagNumber(7)
  void clearLeadingTag() => clearField(7);
}

class Prediction extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Prediction', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'epoch', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'isDeparture', protoName: 'isDeparture')
    ..aOB(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'affectedByLayover', protoName: 'affectedByLayover')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tripTag', protoName: 'tripTag')
    ..pc<Message>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messages', $pb.PbFieldType.PM, subBuilder: Message.create)
    ..aOM<Direction>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'dir', subBuilder: Direction.create)
    ..aOM<Vehicle>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'v', subBuilder: Vehicle.create)
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'vic')
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'block')
    ..hasRequiredFields = false
  ;

  Prediction._() : super();
  factory Prediction({
    $fixnum.Int64? epoch,
    $core.bool? isDeparture,
    $core.bool? affectedByLayover,
    $core.String? tripTag,
    $core.Iterable<Message>? messages,
    Direction? dir,
    Vehicle? v,
    $core.String? vic,
    $core.String? block,
  }) {
    final _result = create();
    if (epoch != null) {
      _result.epoch = epoch;
    }
    if (isDeparture != null) {
      _result.isDeparture = isDeparture;
    }
    if (affectedByLayover != null) {
      _result.affectedByLayover = affectedByLayover;
    }
    if (tripTag != null) {
      _result.tripTag = tripTag;
    }
    if (messages != null) {
      _result.messages.addAll(messages);
    }
    if (dir != null) {
      _result.dir = dir;
    }
    if (v != null) {
      _result.v = v;
    }
    if (vic != null) {
      _result.vic = vic;
    }
    if (block != null) {
      _result.block = block;
    }
    return _result;
  }
  factory Prediction.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Prediction.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Prediction clone() => Prediction()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Prediction copyWith(void Function(Prediction) updates) => super.copyWith((message) => updates(message as Prediction)) as Prediction; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Prediction create() => Prediction._();
  Prediction createEmptyInstance() => create();
  static $pb.PbList<Prediction> createRepeated() => $pb.PbList<Prediction>();
  @$core.pragma('dart2js:noInline')
  static Prediction getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Prediction>(create);
  static Prediction? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get epoch => $_getI64(0);
  @$pb.TagNumber(1)
  set epoch($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEpoch() => $_has(0);
  @$pb.TagNumber(1)
  void clearEpoch() => clearField(1);

  @$pb.TagNumber(3)
  $core.bool get isDeparture => $_getBF(1);
  @$pb.TagNumber(3)
  set isDeparture($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(3)
  $core.bool hasIsDeparture() => $_has(1);
  @$pb.TagNumber(3)
  void clearIsDeparture() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get affectedByLayover => $_getBF(2);
  @$pb.TagNumber(4)
  set affectedByLayover($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(4)
  $core.bool hasAffectedByLayover() => $_has(2);
  @$pb.TagNumber(4)
  void clearAffectedByLayover() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get tripTag => $_getSZ(3);
  @$pb.TagNumber(5)
  set tripTag($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(5)
  $core.bool hasTripTag() => $_has(3);
  @$pb.TagNumber(5)
  void clearTripTag() => clearField(5);

  @$pb.TagNumber(6)
  $core.List<Message> get messages => $_getList(4);

  @$pb.TagNumber(7)
  Direction get dir => $_getN(5);
  @$pb.TagNumber(7)
  set dir(Direction v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasDir() => $_has(5);
  @$pb.TagNumber(7)
  void clearDir() => clearField(7);
  @$pb.TagNumber(7)
  Direction ensureDir() => $_ensure(5);

  @$pb.TagNumber(8)
  Vehicle get v => $_getN(6);
  @$pb.TagNumber(8)
  set v(Vehicle v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasV() => $_has(6);
  @$pb.TagNumber(8)
  void clearV() => clearField(8);
  @$pb.TagNumber(8)
  Vehicle ensureV() => $_ensure(6);

  @$pb.TagNumber(9)
  $core.String get vic => $_getSZ(7);
  @$pb.TagNumber(9)
  set vic($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(9)
  $core.bool hasVic() => $_has(7);
  @$pb.TagNumber(9)
  void clearVic() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get block => $_getSZ(8);
  @$pb.TagNumber(10)
  set block($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(10)
  $core.bool hasBlock() => $_has(8);
  @$pb.TagNumber(10)
  void clearBlock() => clearField(10);
}

class Message extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Message', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'text')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textTTS', protoName: 'textTTS')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textSMS', protoName: 'textSMS')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'textAlt', protoName: 'textAlt')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'creator')
    ..a<$core.int>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'priority', $pb.PbFieldType.O3)
    ..aOB(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sendToBuses', protoName: 'sendToBuses')
    ..pc<MessageRoute>(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'forRoutes', $pb.PbFieldType.PM, protoName: 'forRoutes', subBuilder: MessageRoute.create)
    ..pc<TimeWindow>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'timeWindow', $pb.PbFieldType.PM, protoName: 'timeWindow', subBuilder: TimeWindow.create)
    ..hasRequiredFields = false
  ;

  Message._() : super();
  factory Message({
    $core.String? tag,
    $core.String? text,
    $core.String? textTTS,
    $core.String? textSMS,
    $core.String? textAlt,
    $core.String? creator,
    $core.int? priority,
    $core.bool? sendToBuses,
    $core.Iterable<MessageRoute>? forRoutes,
    $core.Iterable<TimeWindow>? timeWindow,
  }) {
    final _result = create();
    if (tag != null) {
      _result.tag = tag;
    }
    if (text != null) {
      _result.text = text;
    }
    if (textTTS != null) {
      _result.textTTS = textTTS;
    }
    if (textSMS != null) {
      _result.textSMS = textSMS;
    }
    if (textAlt != null) {
      _result.textAlt = textAlt;
    }
    if (creator != null) {
      _result.creator = creator;
    }
    if (priority != null) {
      _result.priority = priority;
    }
    if (sendToBuses != null) {
      _result.sendToBuses = sendToBuses;
    }
    if (forRoutes != null) {
      _result.forRoutes.addAll(forRoutes);
    }
    if (timeWindow != null) {
      _result.timeWindow.addAll(timeWindow);
    }
    return _result;
  }
  factory Message.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Message.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Message clone() => Message()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Message copyWith(void Function(Message) updates) => super.copyWith((message) => updates(message as Message)) as Message; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Message create() => Message._();
  Message createEmptyInstance() => create();
  static $pb.PbList<Message> createRepeated() => $pb.PbList<Message>();
  @$core.pragma('dart2js:noInline')
  static Message getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Message>(create);
  static Message? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get tag => $_getSZ(0);
  @$pb.TagNumber(1)
  set tag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get text => $_getSZ(1);
  @$pb.TagNumber(2)
  set text($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasText() => $_has(1);
  @$pb.TagNumber(2)
  void clearText() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get textTTS => $_getSZ(2);
  @$pb.TagNumber(3)
  set textTTS($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTextTTS() => $_has(2);
  @$pb.TagNumber(3)
  void clearTextTTS() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get textSMS => $_getSZ(3);
  @$pb.TagNumber(4)
  set textSMS($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTextSMS() => $_has(3);
  @$pb.TagNumber(4)
  void clearTextSMS() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get textAlt => $_getSZ(4);
  @$pb.TagNumber(5)
  set textAlt($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTextAlt() => $_has(4);
  @$pb.TagNumber(5)
  void clearTextAlt() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get creator => $_getSZ(5);
  @$pb.TagNumber(6)
  set creator($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasCreator() => $_has(5);
  @$pb.TagNumber(6)
  void clearCreator() => clearField(6);

  @$pb.TagNumber(7)
  $core.int get priority => $_getIZ(6);
  @$pb.TagNumber(7)
  set priority($core.int v) { $_setSignedInt32(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasPriority() => $_has(6);
  @$pb.TagNumber(7)
  void clearPriority() => clearField(7);

  @$pb.TagNumber(8)
  $core.bool get sendToBuses => $_getBF(7);
  @$pb.TagNumber(8)
  set sendToBuses($core.bool v) { $_setBool(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasSendToBuses() => $_has(7);
  @$pb.TagNumber(8)
  void clearSendToBuses() => clearField(8);

  @$pb.TagNumber(9)
  $core.List<MessageRoute> get forRoutes => $_getList(8);

  @$pb.TagNumber(10)
  $core.List<TimeWindow> get timeWindow => $_getList(9);
}

class MessageRoute extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MessageRoute', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'targetRouteTag', protoName: 'targetRouteTag')
    ..pc<Stop>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stops', $pb.PbFieldType.PM, subBuilder: Stop.create)
    ..hasRequiredFields = false
  ;

  MessageRoute._() : super();
  factory MessageRoute({
    $core.String? targetRouteTag,
    $core.Iterable<Stop>? stops,
  }) {
    final _result = create();
    if (targetRouteTag != null) {
      _result.targetRouteTag = targetRouteTag;
    }
    if (stops != null) {
      _result.stops.addAll(stops);
    }
    return _result;
  }
  factory MessageRoute.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MessageRoute.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MessageRoute clone() => MessageRoute()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MessageRoute copyWith(void Function(MessageRoute) updates) => super.copyWith((message) => updates(message as MessageRoute)) as MessageRoute; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MessageRoute create() => MessageRoute._();
  MessageRoute createEmptyInstance() => create();
  static $pb.PbList<MessageRoute> createRepeated() => $pb.PbList<MessageRoute>();
  @$core.pragma('dart2js:noInline')
  static MessageRoute getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MessageRoute>(create);
  static MessageRoute? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get targetRouteTag => $_getSZ(0);
  @$pb.TagNumber(1)
  set targetRouteTag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTargetRouteTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearTargetRouteTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<Stop> get stops => $_getList(1);
}

class RouteStopTag extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RouteStopTag', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'routeTag', protoName: 'routeTag')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'stopTag', protoName: 'stopTag')
    ..hasRequiredFields = false
  ;

  RouteStopTag._() : super();
  factory RouteStopTag({
    $core.String? routeTag,
    $core.String? stopTag,
  }) {
    final _result = create();
    if (routeTag != null) {
      _result.routeTag = routeTag;
    }
    if (stopTag != null) {
      _result.stopTag = stopTag;
    }
    return _result;
  }
  factory RouteStopTag.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RouteStopTag.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RouteStopTag clone() => RouteStopTag()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RouteStopTag copyWith(void Function(RouteStopTag) updates) => super.copyWith((message) => updates(message as RouteStopTag)) as RouteStopTag; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RouteStopTag create() => RouteStopTag._();
  RouteStopTag createEmptyInstance() => create();
  static $pb.PbList<RouteStopTag> createRepeated() => $pb.PbList<RouteStopTag>();
  @$core.pragma('dart2js:noInline')
  static RouteStopTag getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RouteStopTag>(create);
  static RouteStopTag? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get routeTag => $_getSZ(0);
  @$pb.TagNumber(1)
  set routeTag($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRouteTag() => $_has(0);
  @$pb.TagNumber(1)
  void clearRouteTag() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get stopTag => $_getSZ(1);
  @$pb.TagNumber(2)
  set stopTag($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStopTag() => $_has(1);
  @$pb.TagNumber(2)
  void clearStopTag() => clearField(2);
}

class TimeWindow extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TimeWindow', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'proto'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startDay', $pb.PbFieldType.OU3, protoName: 'startDay')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'endDay', $pb.PbFieldType.OU3, protoName: 'endDay')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'startTime', $pb.PbFieldType.OU3, protoName: 'startTime')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'endTime', $pb.PbFieldType.OU3, protoName: 'endTime')
    ..hasRequiredFields = false
  ;

  TimeWindow._() : super();
  factory TimeWindow({
    $core.int? startDay,
    $core.int? endDay,
    $core.int? startTime,
    $core.int? endTime,
  }) {
    final _result = create();
    if (startDay != null) {
      _result.startDay = startDay;
    }
    if (endDay != null) {
      _result.endDay = endDay;
    }
    if (startTime != null) {
      _result.startTime = startTime;
    }
    if (endTime != null) {
      _result.endTime = endTime;
    }
    return _result;
  }
  factory TimeWindow.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TimeWindow.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TimeWindow clone() => TimeWindow()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TimeWindow copyWith(void Function(TimeWindow) updates) => super.copyWith((message) => updates(message as TimeWindow)) as TimeWindow; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TimeWindow create() => TimeWindow._();
  TimeWindow createEmptyInstance() => create();
  static $pb.PbList<TimeWindow> createRepeated() => $pb.PbList<TimeWindow>();
  @$core.pragma('dart2js:noInline')
  static TimeWindow getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TimeWindow>(create);
  static TimeWindow? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get startDay => $_getIZ(0);
  @$pb.TagNumber(1)
  set startDay($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasStartDay() => $_has(0);
  @$pb.TagNumber(1)
  void clearStartDay() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get endDay => $_getIZ(1);
  @$pb.TagNumber(2)
  set endDay($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEndDay() => $_has(1);
  @$pb.TagNumber(2)
  void clearEndDay() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get startTime => $_getIZ(2);
  @$pb.TagNumber(3)
  set startTime($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasStartTime() => $_has(2);
  @$pb.TagNumber(3)
  void clearStartTime() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get endTime => $_getIZ(3);
  @$pb.TagNumber(4)
  set endTime($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasEndTime() => $_has(3);
  @$pb.TagNumber(4)
  void clearEndTime() => clearField(4);
}

