///
//  Generated code. Do not modify.
//  source: main.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use nothingDescriptor instead')
const Nothing$json = const {
  '1': 'Nothing',
};

/// Descriptor for `Nothing`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nothingDescriptor = $convert.base64Decode('CgdOb3RoaW5n');
@$core.Deprecated('Use nameRespDescriptor instead')
const NameResp$json = const {
  '1': 'NameResp',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'version', '3': 2, '4': 1, '5': 9, '10': 'version'},
  ],
};

/// Descriptor for `NameResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nameRespDescriptor = $convert.base64Decode('CghOYW1lUmVzcBISCgRuYW1lGAEgASgJUgRuYW1lEhgKB3ZlcnNpb24YAiABKAlSB3ZlcnNpb24=');
@$core.Deprecated('Use agenciesRespDescriptor instead')
const AgenciesResp$json = const {
  '1': 'AgenciesResp',
  '2': const [
    const {'1': 'licensing', '3': 1, '4': 1, '5': 9, '10': 'licensing'},
    const {'1': 'agencies', '3': 2, '4': 3, '5': 11, '6': '.proto.Agency', '10': 'agencies'},
  ],
};

/// Descriptor for `AgenciesResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agenciesRespDescriptor = $convert.base64Decode('CgxBZ2VuY2llc1Jlc3ASHAoJbGljZW5zaW5nGAEgASgJUglsaWNlbnNpbmcSKQoIYWdlbmNpZXMYAiADKAsyDS5wcm90by5BZ2VuY3lSCGFnZW5jaWVz');
@$core.Deprecated('Use routesReqDescriptor instead')
const RoutesReq$json = const {
  '1': 'RoutesReq',
  '2': const [
    const {'1': 'agencyTag', '3': 1, '4': 1, '5': 9, '10': 'agencyTag'},
  ],
};

/// Descriptor for `RoutesReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List routesReqDescriptor = $convert.base64Decode('CglSb3V0ZXNSZXESHAoJYWdlbmN5VGFnGAEgASgJUglhZ2VuY3lUYWc=');
@$core.Deprecated('Use routesRespDescriptor instead')
const RoutesResp$json = const {
  '1': 'RoutesResp',
  '2': const [
    const {'1': 'licensing', '3': 1, '4': 1, '5': 9, '10': 'licensing'},
    const {'1': 'routes', '3': 2, '4': 3, '5': 11, '6': '.proto.Route', '10': 'routes'},
  ],
};

/// Descriptor for `RoutesResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List routesRespDescriptor = $convert.base64Decode('CgpSb3V0ZXNSZXNwEhwKCWxpY2Vuc2luZxgBIAEoCVIJbGljZW5zaW5nEiQKBnJvdXRlcxgCIAMoCzIMLnByb3RvLlJvdXRlUgZyb3V0ZXM=');
@$core.Deprecated('Use routeReqDescriptor instead')
const RouteReq$json = const {
  '1': 'RouteReq',
  '2': const [
    const {'1': 'agencyTag', '3': 1, '4': 1, '5': 9, '10': 'agencyTag'},
    const {'1': 'routeTag', '3': 2, '4': 1, '5': 9, '10': 'routeTag'},
  ],
};

/// Descriptor for `RouteReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List routeReqDescriptor = $convert.base64Decode('CghSb3V0ZVJlcRIcCglhZ2VuY3lUYWcYASABKAlSCWFnZW5jeVRhZxIaCghyb3V0ZVRhZxgCIAEoCVIIcm91dGVUYWc=');
@$core.Deprecated('Use routeRespDescriptor instead')
const RouteResp$json = const {
  '1': 'RouteResp',
  '2': const [
    const {'1': 'licensing', '3': 1, '4': 1, '5': 9, '10': 'licensing'},
    const {'1': 'route', '3': 2, '4': 1, '5': 11, '6': '.proto.Route', '10': 'route'},
  ],
};

/// Descriptor for `RouteResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List routeRespDescriptor = $convert.base64Decode('CglSb3V0ZVJlc3ASHAoJbGljZW5zaW5nGAEgASgJUglsaWNlbnNpbmcSIgoFcm91dGUYAiABKAsyDC5wcm90by5Sb3V0ZVIFcm91dGU=');
@$core.Deprecated('Use locationsRouteReqDescriptor instead')
const LocationsRouteReq$json = const {
  '1': 'LocationsRouteReq',
  '2': const [
    const {'1': 'agencyTag', '3': 1, '4': 1, '5': 9, '10': 'agencyTag'},
    const {'1': 'routeTag', '3': 2, '4': 1, '5': 9, '10': 'routeTag'},
    const {'1': 't', '3': 3, '4': 1, '5': 9, '10': 't'},
  ],
};

/// Descriptor for `LocationsRouteReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationsRouteReqDescriptor = $convert.base64Decode('ChFMb2NhdGlvbnNSb3V0ZVJlcRIcCglhZ2VuY3lUYWcYASABKAlSCWFnZW5jeVRhZxIaCghyb3V0ZVRhZxgCIAEoCVIIcm91dGVUYWcSDAoBdBgDIAEoCVIBdA==');
@$core.Deprecated('Use locationsReqDescriptor instead')
const LocationsReq$json = const {
  '1': 'LocationsReq',
  '2': const [
    const {'1': 'agencyTag', '3': 1, '4': 1, '5': 9, '10': 'agencyTag'},
    const {'1': 'vehicleTags', '3': 2, '4': 3, '5': 9, '10': 'vehicleTags'},
  ],
};

/// Descriptor for `LocationsReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationsReqDescriptor = $convert.base64Decode('CgxMb2NhdGlvbnNSZXESHAoJYWdlbmN5VGFnGAEgASgJUglhZ2VuY3lUYWcSIAoLdmVoaWNsZVRhZ3MYAiADKAlSC3ZlaGljbGVUYWdz');
@$core.Deprecated('Use locationsRespDescriptor instead')
const LocationsResp$json = const {
  '1': 'LocationsResp',
  '2': const [
    const {'1': 'licensing', '3': 1, '4': 1, '5': 9, '10': 'licensing'},
    const {'1': 'locations', '3': 2, '4': 3, '5': 11, '6': '.proto.VehicleLocation', '10': 'locations'},
  ],
};

/// Descriptor for `LocationsResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationsRespDescriptor = $convert.base64Decode('Cg1Mb2NhdGlvbnNSZXNwEhwKCWxpY2Vuc2luZxgBIAEoCVIJbGljZW5zaW5nEjQKCWxvY2F0aW9ucxgCIAMoCzIWLnByb3RvLlZlaGljbGVMb2NhdGlvblIJbG9jYXRpb25z');
@$core.Deprecated('Use predictionsReqDescriptor instead')
const PredictionsReq$json = const {
  '1': 'PredictionsReq',
  '2': const [
    const {'1': 'agencyTag', '3': 1, '4': 1, '5': 9, '10': 'agencyTag'},
    const {'1': 'rsTags', '3': 2, '4': 3, '5': 11, '6': '.proto.RouteStopTag', '10': 'rsTags'},
  ],
};

/// Descriptor for `PredictionsReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List predictionsReqDescriptor = $convert.base64Decode('Cg5QcmVkaWN0aW9uc1JlcRIcCglhZ2VuY3lUYWcYASABKAlSCWFnZW5jeVRhZxIrCgZyc1RhZ3MYAiADKAsyEy5wcm90by5Sb3V0ZVN0b3BUYWdSBnJzVGFncw==');
@$core.Deprecated('Use predictionsRespDescriptor instead')
const PredictionsResp$json = const {
  '1': 'PredictionsResp',
  '2': const [
    const {'1': 'licensing', '3': 1, '4': 1, '5': 9, '10': 'licensing'},
    const {'1': 'predictions', '3': 2, '4': 3, '5': 11, '6': '.proto.Prediction', '10': 'predictions'},
  ],
};

/// Descriptor for `PredictionsResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List predictionsRespDescriptor = $convert.base64Decode('Cg9QcmVkaWN0aW9uc1Jlc3ASHAoJbGljZW5zaW5nGAEgASgJUglsaWNlbnNpbmcSMwoLcHJlZGljdGlvbnMYAiADKAsyES5wcm90by5QcmVkaWN0aW9uUgtwcmVkaWN0aW9ucw==');
@$core.Deprecated('Use messagesReqDescriptor instead')
const MessagesReq$json = const {
  '1': 'MessagesReq',
  '2': const [
    const {'1': 'agencyTag', '3': 1, '4': 1, '5': 9, '10': 'agencyTag'},
    const {'1': 'routeTags', '3': 2, '4': 3, '5': 9, '10': 'routeTags'},
  ],
};

/// Descriptor for `MessagesReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messagesReqDescriptor = $convert.base64Decode('CgtNZXNzYWdlc1JlcRIcCglhZ2VuY3lUYWcYASABKAlSCWFnZW5jeVRhZxIcCglyb3V0ZVRhZ3MYAiADKAlSCXJvdXRlVGFncw==');
@$core.Deprecated('Use messagesRespDescriptor instead')
const MessagesResp$json = const {
  '1': 'MessagesResp',
  '2': const [
    const {'1': 'licensing', '3': 1, '4': 1, '5': 9, '10': 'licensing'},
    const {'1': 'messages', '3': 2, '4': 3, '5': 11, '6': '.proto.Message', '10': 'messages'},
  ],
};

/// Descriptor for `MessagesResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messagesRespDescriptor = $convert.base64Decode('CgxNZXNzYWdlc1Jlc3ASHAoJbGljZW5zaW5nGAEgASgJUglsaWNlbnNpbmcSKgoIbWVzc2FnZXMYAiADKAsyDi5wcm90by5NZXNzYWdlUghtZXNzYWdlcw==');
@$core.Deprecated('Use agencyDescriptor instead')
const Agency$json = const {
  '1': 'Agency',
  '2': const [
    const {'1': 'tag', '3': 1, '4': 1, '5': 9, '10': 'tag'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'region', '3': 3, '4': 1, '5': 9, '10': 'region'},
    const {'1': 'short', '3': 4, '4': 1, '5': 9, '10': 'short'},
    const {'1': 'routes', '3': 5, '4': 3, '5': 11, '6': '.proto.Route', '10': 'routes'},
  ],
};

/// Descriptor for `Agency`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agencyDescriptor = $convert.base64Decode('CgZBZ2VuY3kSEAoDdGFnGAEgASgJUgN0YWcSFAoFdGl0bGUYAiABKAlSBXRpdGxlEhYKBnJlZ2lvbhgDIAEoCVIGcmVnaW9uEhQKBXNob3J0GAQgASgJUgVzaG9ydBIkCgZyb3V0ZXMYBSADKAsyDC5wcm90by5Sb3V0ZVIGcm91dGVz');
@$core.Deprecated('Use routeDescriptor instead')
const Route$json = const {
  '1': 'Route',
  '2': const [
    const {'1': 'tag', '3': 1, '4': 1, '5': 9, '10': 'tag'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'color', '3': 3, '4': 1, '5': 9, '10': 'color'},
    const {'1': 'colorOpposite', '3': 4, '4': 1, '5': 9, '10': 'colorOpposite'},
    const {'1': 'min', '3': 5, '4': 1, '5': 11, '6': '.proto.Point', '10': 'min'},
    const {'1': 'max', '3': 6, '4': 1, '5': 11, '6': '.proto.Point', '10': 'max'},
    const {'1': 'stops', '3': 7, '4': 3, '5': 11, '6': '.proto.Stop', '10': 'stops'},
    const {'1': 'directions', '3': 8, '4': 3, '5': 11, '6': '.proto.Direction', '10': 'directions'},
    const {'1': 'paths', '3': 9, '4': 3, '5': 11, '6': '.proto.Path', '10': 'paths'},
  ],
};

/// Descriptor for `Route`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List routeDescriptor = $convert.base64Decode('CgVSb3V0ZRIQCgN0YWcYASABKAlSA3RhZxIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSFAoFY29sb3IYAyABKAlSBWNvbG9yEiQKDWNvbG9yT3Bwb3NpdGUYBCABKAlSDWNvbG9yT3Bwb3NpdGUSHgoDbWluGAUgASgLMgwucHJvdG8uUG9pbnRSA21pbhIeCgNtYXgYBiABKAsyDC5wcm90by5Qb2ludFIDbWF4EiEKBXN0b3BzGAcgAygLMgsucHJvdG8uU3RvcFIFc3RvcHMSMAoKZGlyZWN0aW9ucxgIIAMoCzIQLnByb3RvLkRpcmVjdGlvblIKZGlyZWN0aW9ucxIhCgVwYXRocxgJIAMoCzILLnByb3RvLlBhdGhSBXBhdGhz');
@$core.Deprecated('Use stopDescriptor instead')
const Stop$json = const {
  '1': 'Stop',
  '2': const [
    const {'1': 'tag', '3': 1, '4': 1, '5': 9, '10': 'tag'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'id', '3': 3, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'pos', '3': 4, '4': 1, '5': 11, '6': '.proto.Point', '10': 'pos'},
  ],
};

/// Descriptor for `Stop`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List stopDescriptor = $convert.base64Decode('CgRTdG9wEhAKA3RhZxgBIAEoCVIDdGFnEhQKBXRpdGxlGAIgASgJUgV0aXRsZRIOCgJpZBgDIAEoCVICaWQSHgoDcG9zGAQgASgLMgwucHJvdG8uUG9pbnRSA3Bvcw==');
@$core.Deprecated('Use directionDescriptor instead')
const Direction$json = const {
  '1': 'Direction',
  '2': const [
    const {'1': 'tag', '3': 1, '4': 1, '5': 9, '10': 'tag'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'branch', '3': 4, '4': 1, '5': 9, '10': 'branch'},
    const {'1': 'useForUI', '3': 5, '4': 1, '5': 8, '10': 'useForUI'},
    const {'1': 'stops', '3': 6, '4': 3, '5': 11, '6': '.proto.Stop', '10': 'stops'},
  ],
};

/// Descriptor for `Direction`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List directionDescriptor = $convert.base64Decode('CglEaXJlY3Rpb24SEAoDdGFnGAEgASgJUgN0YWcSFAoFdGl0bGUYAiABKAlSBXRpdGxlEhIKBG5hbWUYAyABKAlSBG5hbWUSFgoGYnJhbmNoGAQgASgJUgZicmFuY2gSGgoIdXNlRm9yVUkYBSABKAhSCHVzZUZvclVJEiEKBXN0b3BzGAYgAygLMgsucHJvdG8uU3RvcFIFc3RvcHM=');
@$core.Deprecated('Use pathDescriptor instead')
const Path$json = const {
  '1': 'Path',
  '2': const [
    const {'1': 'points', '3': 1, '4': 3, '5': 11, '6': '.proto.Point', '10': 'points'},
  ],
};

/// Descriptor for `Path`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pathDescriptor = $convert.base64Decode('CgRQYXRoEiQKBnBvaW50cxgBIAMoCzIMLnByb3RvLlBvaW50UgZwb2ludHM=');
@$core.Deprecated('Use pointDescriptor instead')
const Point$json = const {
  '1': 'Point',
  '2': const [
    const {'1': 'lat', '3': 1, '4': 1, '5': 2, '10': 'lat'},
    const {'1': 'lon', '3': 2, '4': 1, '5': 2, '10': 'lon'},
  ],
};

/// Descriptor for `Point`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pointDescriptor = $convert.base64Decode('CgVQb2ludBIQCgNsYXQYASABKAJSA2xhdBIQCgNsb24YAiABKAJSA2xvbg==');
@$core.Deprecated('Use vehicleDescriptor instead')
const Vehicle$json = const {
  '1': 'Vehicle',
  '2': const [
    const {'1': 'tag', '3': 1, '4': 1, '5': 9, '10': 'tag'},
    const {'1': 'routeTag', '3': 2, '4': 1, '5': 9, '10': 'routeTag'},
    const {'1': 'dirTag', '3': 3, '4': 1, '5': 9, '10': 'dirTag'},
  ],
};

/// Descriptor for `Vehicle`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List vehicleDescriptor = $convert.base64Decode('CgdWZWhpY2xlEhAKA3RhZxgBIAEoCVIDdGFnEhoKCHJvdXRlVGFnGAIgASgJUghyb3V0ZVRhZxIWCgZkaXJUYWcYAyABKAlSBmRpclRhZw==');
@$core.Deprecated('Use vehicleLocationDescriptor instead')
const VehicleLocation$json = const {
  '1': 'VehicleLocation',
  '2': const [
    const {'1': 'target', '3': 1, '4': 1, '5': 11, '6': '.proto.Vehicle', '10': 'target'},
    const {'1': 'location', '3': 2, '4': 1, '5': 11, '6': '.proto.Point', '10': 'location'},
    const {'1': 'secsSinceReport', '3': 3, '4': 1, '5': 5, '10': 'secsSinceReport'},
    const {'1': 'predictable', '3': 4, '4': 1, '5': 8, '10': 'predictable'},
    const {'1': 'heading', '3': 5, '4': 1, '5': 5, '10': 'heading'},
    const {'1': 'speed', '3': 6, '4': 1, '5': 5, '10': 'speed'},
    const {'1': 'leadingTag', '3': 7, '4': 1, '5': 9, '10': 'leadingTag'},
  ],
};

/// Descriptor for `VehicleLocation`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List vehicleLocationDescriptor = $convert.base64Decode('Cg9WZWhpY2xlTG9jYXRpb24SJgoGdGFyZ2V0GAEgASgLMg4ucHJvdG8uVmVoaWNsZVIGdGFyZ2V0EigKCGxvY2F0aW9uGAIgASgLMgwucHJvdG8uUG9pbnRSCGxvY2F0aW9uEigKD3NlY3NTaW5jZVJlcG9ydBgDIAEoBVIPc2Vjc1NpbmNlUmVwb3J0EiAKC3ByZWRpY3RhYmxlGAQgASgIUgtwcmVkaWN0YWJsZRIYCgdoZWFkaW5nGAUgASgFUgdoZWFkaW5nEhQKBXNwZWVkGAYgASgFUgVzcGVlZBIeCgpsZWFkaW5nVGFnGAcgASgJUgpsZWFkaW5nVGFn');
@$core.Deprecated('Use predictionDescriptor instead')
const Prediction$json = const {
  '1': 'Prediction',
  '2': const [
    const {'1': 'epoch', '3': 1, '4': 1, '5': 4, '10': 'epoch'},
    const {'1': 'isDeparture', '3': 3, '4': 1, '5': 8, '10': 'isDeparture'},
    const {'1': 'affectedByLayover', '3': 4, '4': 1, '5': 8, '10': 'affectedByLayover'},
    const {'1': 'tripTag', '3': 5, '4': 1, '5': 9, '10': 'tripTag'},
    const {'1': 'messages', '3': 6, '4': 3, '5': 11, '6': '.proto.Message', '10': 'messages'},
    const {'1': 'dir', '3': 7, '4': 1, '5': 11, '6': '.proto.Direction', '10': 'dir'},
    const {'1': 'v', '3': 8, '4': 1, '5': 11, '6': '.proto.Vehicle', '10': 'v'},
    const {'1': 'vic', '3': 9, '4': 1, '5': 9, '10': 'vic'},
    const {'1': 'block', '3': 10, '4': 1, '5': 9, '10': 'block'},
  ],
};

/// Descriptor for `Prediction`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List predictionDescriptor = $convert.base64Decode('CgpQcmVkaWN0aW9uEhQKBWVwb2NoGAEgASgEUgVlcG9jaBIgCgtpc0RlcGFydHVyZRgDIAEoCFILaXNEZXBhcnR1cmUSLAoRYWZmZWN0ZWRCeUxheW92ZXIYBCABKAhSEWFmZmVjdGVkQnlMYXlvdmVyEhgKB3RyaXBUYWcYBSABKAlSB3RyaXBUYWcSKgoIbWVzc2FnZXMYBiADKAsyDi5wcm90by5NZXNzYWdlUghtZXNzYWdlcxIiCgNkaXIYByABKAsyEC5wcm90by5EaXJlY3Rpb25SA2RpchIcCgF2GAggASgLMg4ucHJvdG8uVmVoaWNsZVIBdhIQCgN2aWMYCSABKAlSA3ZpYxIUCgVibG9jaxgKIAEoCVIFYmxvY2s=');
@$core.Deprecated('Use messageDescriptor instead')
const Message$json = const {
  '1': 'Message',
  '2': const [
    const {'1': 'tag', '3': 1, '4': 1, '5': 9, '10': 'tag'},
    const {'1': 'text', '3': 2, '4': 1, '5': 9, '10': 'text'},
    const {'1': 'textTTS', '3': 3, '4': 1, '5': 9, '10': 'textTTS'},
    const {'1': 'textSMS', '3': 4, '4': 1, '5': 9, '10': 'textSMS'},
    const {'1': 'textAlt', '3': 5, '4': 1, '5': 9, '10': 'textAlt'},
    const {'1': 'creator', '3': 6, '4': 1, '5': 9, '10': 'creator'},
    const {'1': 'priority', '3': 7, '4': 1, '5': 5, '10': 'priority'},
    const {'1': 'sendToBuses', '3': 8, '4': 1, '5': 8, '10': 'sendToBuses'},
    const {'1': 'forRoutes', '3': 9, '4': 3, '5': 11, '6': '.proto.MessageRoute', '10': 'forRoutes'},
    const {'1': 'timeWindow', '3': 10, '4': 3, '5': 11, '6': '.proto.TimeWindow', '10': 'timeWindow'},
  ],
};

/// Descriptor for `Message`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageDescriptor = $convert.base64Decode('CgdNZXNzYWdlEhAKA3RhZxgBIAEoCVIDdGFnEhIKBHRleHQYAiABKAlSBHRleHQSGAoHdGV4dFRUUxgDIAEoCVIHdGV4dFRUUxIYCgd0ZXh0U01TGAQgASgJUgd0ZXh0U01TEhgKB3RleHRBbHQYBSABKAlSB3RleHRBbHQSGAoHY3JlYXRvchgGIAEoCVIHY3JlYXRvchIaCghwcmlvcml0eRgHIAEoBVIIcHJpb3JpdHkSIAoLc2VuZFRvQnVzZXMYCCABKAhSC3NlbmRUb0J1c2VzEjEKCWZvclJvdXRlcxgJIAMoCzITLnByb3RvLk1lc3NhZ2VSb3V0ZVIJZm9yUm91dGVzEjEKCnRpbWVXaW5kb3cYCiADKAsyES5wcm90by5UaW1lV2luZG93Ugp0aW1lV2luZG93');
@$core.Deprecated('Use messageRouteDescriptor instead')
const MessageRoute$json = const {
  '1': 'MessageRoute',
  '2': const [
    const {'1': 'targetRouteTag', '3': 1, '4': 1, '5': 9, '10': 'targetRouteTag'},
    const {'1': 'stops', '3': 2, '4': 3, '5': 11, '6': '.proto.Stop', '10': 'stops'},
  ],
};

/// Descriptor for `MessageRoute`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageRouteDescriptor = $convert.base64Decode('CgxNZXNzYWdlUm91dGUSJgoOdGFyZ2V0Um91dGVUYWcYASABKAlSDnRhcmdldFJvdXRlVGFnEiEKBXN0b3BzGAIgAygLMgsucHJvdG8uU3RvcFIFc3RvcHM=');
@$core.Deprecated('Use routeStopTagDescriptor instead')
const RouteStopTag$json = const {
  '1': 'RouteStopTag',
  '2': const [
    const {'1': 'routeTag', '3': 1, '4': 1, '5': 9, '10': 'routeTag'},
    const {'1': 'stopTag', '3': 2, '4': 1, '5': 9, '10': 'stopTag'},
  ],
};

/// Descriptor for `RouteStopTag`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List routeStopTagDescriptor = $convert.base64Decode('CgxSb3V0ZVN0b3BUYWcSGgoIcm91dGVUYWcYASABKAlSCHJvdXRlVGFnEhgKB3N0b3BUYWcYAiABKAlSB3N0b3BUYWc=');
@$core.Deprecated('Use timeWindowDescriptor instead')
const TimeWindow$json = const {
  '1': 'TimeWindow',
  '2': const [
    const {'1': 'startDay', '3': 1, '4': 1, '5': 13, '10': 'startDay'},
    const {'1': 'endDay', '3': 2, '4': 1, '5': 13, '10': 'endDay'},
    const {'1': 'startTime', '3': 3, '4': 1, '5': 13, '10': 'startTime'},
    const {'1': 'endTime', '3': 4, '4': 1, '5': 13, '10': 'endTime'},
  ],
};

/// Descriptor for `TimeWindow`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List timeWindowDescriptor = $convert.base64Decode('CgpUaW1lV2luZG93EhoKCHN0YXJ0RGF5GAEgASgNUghzdGFydERheRIWCgZlbmREYXkYAiABKA1SBmVuZERheRIcCglzdGFydFRpbWUYAyABKA1SCXN0YXJ0VGltZRIYCgdlbmRUaW1lGAQgASgNUgdlbmRUaW1l');
