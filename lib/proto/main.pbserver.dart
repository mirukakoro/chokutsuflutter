///
//  Generated code. Do not modify.
//  source: main.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:async' as $async;

import 'package:protobuf/protobuf.dart' as $pb;

import 'dart:core' as $core;
import 'main.pb.dart' as $0;
import 'main.pbjson.dart';

export 'main.pb.dart';

abstract class DataServiceBase extends $pb.GeneratedService {
  $async.Future<$0.NameResp> name($pb.ServerContext ctx, $0.Nothing request);
  $async.Future<$0.AgenciesResp> agencies($pb.ServerContext ctx, $0.Nothing request);
  $async.Future<$0.RoutesResp> routes($pb.ServerContext ctx, $0.RoutesReq request);
  $async.Future<$0.RouteResp> route($pb.ServerContext ctx, $0.RouteReq request);
  $async.Future<$0.LocationsResp> locationsRoute($pb.ServerContext ctx, $0.LocationsRouteReq request);
  $async.Future<$0.LocationsResp> locations($pb.ServerContext ctx, $0.LocationsReq request);
  $async.Future<$0.PredictionsResp> predictions($pb.ServerContext ctx, $0.PredictionsReq request);
  $async.Future<$0.MessagesResp> messages($pb.ServerContext ctx, $0.MessagesReq request);

  $pb.GeneratedMessage createRequest($core.String method) {
    switch (method) {
      case 'Name': return $0.Nothing();
      case 'Agencies': return $0.Nothing();
      case 'Routes': return $0.RoutesReq();
      case 'Route': return $0.RouteReq();
      case 'LocationsRoute': return $0.LocationsRouteReq();
      case 'Locations': return $0.LocationsReq();
      case 'Predictions': return $0.PredictionsReq();
      case 'Messages': return $0.MessagesReq();
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $async.Future<$pb.GeneratedMessage> handleCall($pb.ServerContext ctx, $core.String method, $pb.GeneratedMessage request) {
    switch (method) {
      case 'Name': return this.name(ctx, request as $0.Nothing);
      case 'Agencies': return this.agencies(ctx, request as $0.Nothing);
      case 'Routes': return this.routes(ctx, request as $0.RoutesReq);
      case 'Route': return this.route(ctx, request as $0.RouteReq);
      case 'LocationsRoute': return this.locationsRoute(ctx, request as $0.LocationsRouteReq);
      case 'Locations': return this.locations(ctx, request as $0.LocationsReq);
      case 'Predictions': return this.predictions(ctx, request as $0.PredictionsReq);
      case 'Messages': return this.messages(ctx, request as $0.MessagesReq);
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $core.Map<$core.String, $core.dynamic> get $json => DataServiceBase$json;
  $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> get $messageJson => DataServiceBase$messageJson;
}

