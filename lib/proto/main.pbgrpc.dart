///
//  Generated code. Do not modify.
//  source: main.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'main.pb.dart' as $0;
export 'main.pb.dart';

class DataClient extends $grpc.Client {
  static final _$name = $grpc.ClientMethod<$0.Nothing, $0.NameResp>(
      '/proto.Data/Name',
      ($0.Nothing value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.NameResp.fromBuffer(value));
  static final _$agencies = $grpc.ClientMethod<$0.Nothing, $0.AgenciesResp>(
      '/proto.Data/Agencies',
      ($0.Nothing value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.AgenciesResp.fromBuffer(value));
  static final _$routes = $grpc.ClientMethod<$0.RoutesReq, $0.RoutesResp>(
      '/proto.Data/Routes',
      ($0.RoutesReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.RoutesResp.fromBuffer(value));
  static final _$route = $grpc.ClientMethod<$0.RouteReq, $0.RouteResp>(
      '/proto.Data/Route',
      ($0.RouteReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.RouteResp.fromBuffer(value));
  static final _$locationsRoute =
      $grpc.ClientMethod<$0.LocationsRouteReq, $0.LocationsResp>(
          '/proto.Data/LocationsRoute',
          ($0.LocationsRouteReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.LocationsResp.fromBuffer(value));
  static final _$locations =
      $grpc.ClientMethod<$0.LocationsReq, $0.LocationsResp>(
          '/proto.Data/Locations',
          ($0.LocationsReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.LocationsResp.fromBuffer(value));
  static final _$predictions =
      $grpc.ClientMethod<$0.PredictionsReq, $0.PredictionsResp>(
          '/proto.Data/Predictions',
          ($0.PredictionsReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.PredictionsResp.fromBuffer(value));
  static final _$messages = $grpc.ClientMethod<$0.MessagesReq, $0.MessagesResp>(
      '/proto.Data/Messages',
      ($0.MessagesReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.MessagesResp.fromBuffer(value));

  DataClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.NameResp> name($0.Nothing request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$name, request, options: options);
  }

  $grpc.ResponseFuture<$0.AgenciesResp> agencies($0.Nothing request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$agencies, request, options: options);
  }

  $grpc.ResponseFuture<$0.RoutesResp> routes($0.RoutesReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$routes, request, options: options);
  }

  $grpc.ResponseFuture<$0.RouteResp> route($0.RouteReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$route, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationsResp> locationsRoute(
      $0.LocationsRouteReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$locationsRoute, request, options: options);
  }

  $grpc.ResponseFuture<$0.LocationsResp> locations($0.LocationsReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$locations, request, options: options);
  }

  $grpc.ResponseFuture<$0.PredictionsResp> predictions(
      $0.PredictionsReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$predictions, request, options: options);
  }

  $grpc.ResponseFuture<$0.MessagesResp> messages($0.MessagesReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$messages, request, options: options);
  }
}

abstract class DataServiceBase extends $grpc.Service {
  $core.String get $name => 'proto.Data';

  DataServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Nothing, $0.NameResp>(
        'Name',
        name_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Nothing.fromBuffer(value),
        ($0.NameResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Nothing, $0.AgenciesResp>(
        'Agencies',
        agencies_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Nothing.fromBuffer(value),
        ($0.AgenciesResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RoutesReq, $0.RoutesResp>(
        'Routes',
        routes_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RoutesReq.fromBuffer(value),
        ($0.RoutesResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RouteReq, $0.RouteResp>(
        'Route',
        route_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RouteReq.fromBuffer(value),
        ($0.RouteResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LocationsRouteReq, $0.LocationsResp>(
        'LocationsRoute',
        locationsRoute_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LocationsRouteReq.fromBuffer(value),
        ($0.LocationsResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LocationsReq, $0.LocationsResp>(
        'Locations',
        locations_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.LocationsReq.fromBuffer(value),
        ($0.LocationsResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PredictionsReq, $0.PredictionsResp>(
        'Predictions',
        predictions_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PredictionsReq.fromBuffer(value),
        ($0.PredictionsResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.MessagesReq, $0.MessagesResp>(
        'Messages',
        messages_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.MessagesReq.fromBuffer(value),
        ($0.MessagesResp value) => value.writeToBuffer()));
  }

  $async.Future<$0.NameResp> name_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Nothing> request) async {
    return name(call, await request);
  }

  $async.Future<$0.AgenciesResp> agencies_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Nothing> request) async {
    return agencies(call, await request);
  }

  $async.Future<$0.RoutesResp> routes_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RoutesReq> request) async {
    return routes(call, await request);
  }

  $async.Future<$0.RouteResp> route_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RouteReq> request) async {
    return route(call, await request);
  }

  $async.Future<$0.LocationsResp> locationsRoute_Pre($grpc.ServiceCall call,
      $async.Future<$0.LocationsRouteReq> request) async {
    return locationsRoute(call, await request);
  }

  $async.Future<$0.LocationsResp> locations_Pre(
      $grpc.ServiceCall call, $async.Future<$0.LocationsReq> request) async {
    return locations(call, await request);
  }

  $async.Future<$0.PredictionsResp> predictions_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PredictionsReq> request) async {
    return predictions(call, await request);
  }

  $async.Future<$0.MessagesResp> messages_Pre(
      $grpc.ServiceCall call, $async.Future<$0.MessagesReq> request) async {
    return messages(call, await request);
  }

  $async.Future<$0.NameResp> name($grpc.ServiceCall call, $0.Nothing request);
  $async.Future<$0.AgenciesResp> agencies(
      $grpc.ServiceCall call, $0.Nothing request);
  $async.Future<$0.RoutesResp> routes(
      $grpc.ServiceCall call, $0.RoutesReq request);
  $async.Future<$0.RouteResp> route(
      $grpc.ServiceCall call, $0.RouteReq request);
  $async.Future<$0.LocationsResp> locationsRoute(
      $grpc.ServiceCall call, $0.LocationsRouteReq request);
  $async.Future<$0.LocationsResp> locations(
      $grpc.ServiceCall call, $0.LocationsReq request);
  $async.Future<$0.PredictionsResp> predictions(
      $grpc.ServiceCall call, $0.PredictionsReq request);
  $async.Future<$0.MessagesResp> messages(
      $grpc.ServiceCall call, $0.MessagesReq request);
}
