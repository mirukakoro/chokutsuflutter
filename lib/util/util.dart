import 'package:url_launcher/url_launcher.dart';

void launchCptdb(String term) async {
  final url =
      "https://cptdb.ca/wiki/index.php?search=${Uri.encodeQueryComponent(term)}";
  if (await canLaunch(url))
    await launch(url);
  else
    throw "Could not launch $url";
}
